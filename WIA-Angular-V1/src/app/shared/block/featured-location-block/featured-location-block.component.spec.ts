import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FeaturedLocationBlockComponent } from './featured-location-block.component';

describe('FeaturedLocationBlockComponent', () => {
  let component: FeaturedLocationBlockComponent;
  let fixture: ComponentFixture<FeaturedLocationBlockComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FeaturedLocationBlockComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FeaturedLocationBlockComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
