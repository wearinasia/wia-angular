import { Component, OnInit, Input} from '@angular/core';
import { CatalogService } from 'src/app/service/catalog.service';

@Component({
  selector: 'app-featured-location-block',
  templateUrl: './featured-location-block.component.html',
  styleUrls: ['./featured-location-block.component.css']
})
export class FeaturedLocationBlockComponent implements OnInit {
  @Input() id = null;

  locations: [];

  constructor(private productService: CatalogService){}

  ngOnInit() {
    this.productService.getProductsByCategoryId(this.id).subscribe(response=>{
      this.locations=response['subcategory'];
    });
  }

}
