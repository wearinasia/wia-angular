import { Component, OnInit, Input } from '@angular/core';
import { CatalogService } from 'src/app/service/catalog.service';

@Component({
  selector: 'app-featured-activity-block',
  templateUrl: './featured-activity-block.component.html',
  styleUrls: ['./featured-activity-block.component.css']
})
export class FeaturedActivityBlockComponent implements OnInit {
  @Input() id = null;

  activity: any[];
  constructor(private productService: CatalogService) { }

  ngOnInit() {
    this.productService.getProductsByCategoryId(this.id).subscribe(response=>{
      var temp: any[]
      temp = response['subcategory']
      this.activity = temp
    });
  }

}
