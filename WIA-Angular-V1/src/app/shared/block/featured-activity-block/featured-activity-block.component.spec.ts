import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FeaturedActivityBlockComponent } from './featured-activity-block.component';

describe('FeaturedActivityBlockComponent', () => {
  let component: FeaturedActivityBlockComponent;
  let fixture: ComponentFixture<FeaturedActivityBlockComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FeaturedActivityBlockComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FeaturedActivityBlockComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
