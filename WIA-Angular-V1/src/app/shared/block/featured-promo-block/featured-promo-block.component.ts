import { Component, OnInit, Input } from '@angular/core';
import { CatalogService } from 'src/app/service/catalog.service';

@Component({
  selector: 'app-featured-promo-block',
  templateUrl: './featured-promo-block.component.html',
  styleUrls: ['./featured-promo-block.component.css']
})
export class FeaturedPromoBlockComponent implements OnInit {
  @Input() id = null;

  promotions: any[];
  constructor(private productService: CatalogService) { }

  ngOnInit() {
    this.productService.getPromoCategory(this.id).subscribe(response=>{
      var temp: any[]
      temp = response['subcategory']
      this.promotions = temp
      console.log(response)
    });
  }

}
