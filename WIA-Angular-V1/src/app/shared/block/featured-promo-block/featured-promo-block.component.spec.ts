import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FeaturedPromoBlockComponent } from './featured-promo-block.component';

describe('FeaturedPromoBlockComponent', () => {
  let component: FeaturedPromoBlockComponent;
  let fixture: ComponentFixture<FeaturedPromoBlockComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FeaturedPromoBlockComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FeaturedPromoBlockComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
