import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FeaturedProductBlockComponent } from './featured-product-block.component';

describe('FeaturedProductBlockComponent', () => {
  let component: FeaturedProductBlockComponent;
  let fixture: ComponentFixture<FeaturedProductBlockComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FeaturedProductBlockComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FeaturedProductBlockComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
