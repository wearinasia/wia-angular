import { Component, OnInit, Input } from '@angular/core';
import { CatalogService } from 'src/app/service/catalog.service';
import { ProductsEntity } from 'src/app/models/category';

@Component({
  selector: 'app-featured-product-block',
  templateUrl: './featured-product-block.component.html',
  styleUrls: ['./featured-product-block.component.css']
})
export class FeaturedProductBlockComponent implements OnInit {
  @Input() id = null;
  products: ProductsEntity[];

  constructor(private productService: CatalogService) { }

  ngOnInit() {
    this.productService.getProductsByCategoryId(this.id).subscribe(response=>{
      var temp = response.products
      
      this.products = temp
    });
  }

}
