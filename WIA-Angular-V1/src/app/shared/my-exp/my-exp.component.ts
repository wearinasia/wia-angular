import { Component, OnInit, Input } from '@angular/core';
import { Product } from '../../models/product'

@Component({
  selector: 'app-my-exp',
  templateUrl: './my-exp.component.html',
  styleUrls: ['./my-exp.component.css']
})
export class MyExpComponent implements OnInit {

  @Input() product: Product;

  constructor() { }

  ngOnInit() {
  }

}
