import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-slider-product-details',
  templateUrl: './slider-product-details.component.html',
  styleUrls: ['./slider-product-details.component.css']
})
export class SliderProductDetailsComponent implements OnInit {
  @Input() images;
  constructor() { }

  ngOnInit() {
  }

}
