import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CardExperienceActivityComponent } from './card-experience-activity.component';

describe('CardExperienceActivityComponent', () => {
  let component: CardExperienceActivityComponent;
  let fixture: ComponentFixture<CardExperienceActivityComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CardExperienceActivityComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CardExperienceActivityComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
