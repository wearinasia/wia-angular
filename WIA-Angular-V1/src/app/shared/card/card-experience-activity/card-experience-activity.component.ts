import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-card-experience-activity',
  templateUrl: './card-experience-activity.component.html',
  styleUrls: ['./card-experience-activity.component.css']
})
export class CardExperienceActivityComponent implements OnInit {
  @Input() title = null || 'name'
  @Input() id = null
  @Input() image= null || 'image'
  @Input() name = null || 'name'
  @Input() desc = null || 'desc'
  constructor() { }

  ngOnInit() {
  }

}
