import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CardExperienceFeaturedProductComponent } from './card-experience-featured-product.component';

describe('CardExperienceFeaturedProductComponent', () => {
  let component: CardExperienceFeaturedProductComponent;
  let fixture: ComponentFixture<CardExperienceFeaturedProductComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CardExperienceFeaturedProductComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CardExperienceFeaturedProductComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
