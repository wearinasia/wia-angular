import { Component, OnInit, Input } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { faStar } from '@fortawesome/free-solid-svg-icons';

@Component({
  selector: 'app-card-experience-featured-product',
  templateUrl: './card-experience-featured-product.component.html',
  styleUrls: ['./card-experience-featured-product.component.css']
})
export class CardExperienceFeaturedProductComponent implements OnInit {
  @Input() id = null || 'id'
  @Input() name = null || 'name'
  @Input() description = null || 'description'
  @Input() thumbnail = null ||  'thumbnail'
  @Input() price = null ||  'price'
  @Input() avatar = null ||  '#'
  @Input() review = null || 'review'
  @Input() numReview = null || 'numReview'
  @Input() label = null || 'label'

  faStar = faStar;

  constructor(private router: Router, private route: ActivatedRoute) {
  }

  ngOnInit() {
    this.name.toLowerCase
    // var nameTemp: string[]
    // nameTemp = this.description.split(" ")
    // nameTemp = nameTemp.slice(0,4)
    // nameTemp.push("...")

    // this.name = nameTemp.join(" ")

    // var descriptionTemp: string[]
    // descriptionTemp = this.description.split(" ")
    // descriptionTemp = descriptionTemp.slice(1)
    // descriptionTemp.push("...")

    // this.description = descriptionTemp.join(" ")
  }

}
