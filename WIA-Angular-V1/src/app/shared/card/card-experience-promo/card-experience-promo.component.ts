import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-card-experience-promo',
  templateUrl: './card-experience-promo.component.html',
  styleUrls: ['./card-experience-promo.component.css']
})
export class CardExperiencePromoComponent implements OnInit {
  @Input() title = null || 'name'
  @Input() id = null
  @Input() image= null || 'image'
  @Input() name = null || 'name'
  @Input() desc = null || 'desc'

  constructor() { }

  ngOnInit() {
  }

}
