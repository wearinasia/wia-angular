import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CardExperiencePromoComponent } from './card-experience-promo.component';

describe('CardExperiencePromoComponent', () => {
  let component: CardExperiencePromoComponent;
  let fixture: ComponentFixture<CardExperiencePromoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CardExperiencePromoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CardExperiencePromoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
