import { Component, OnInit,Input } from '@angular/core';
import { faStar } from '@fortawesome/free-solid-svg-icons';

@Component({
  selector: 'app-card-experience-product',
  templateUrl: './card-experience-product.component.html',
  styleUrls: ['./card-experience-product.component.css']
})
export class CardExperienceProductComponent implements OnInit {
  @Input() label = null || 'label' ;
  @Input() title = null || 'title' ;
  @Input() price = null || 'price' ;
  @Input() image = null || '#' ;
  @Input() avatar = null || '#' ;
  @Input() url = null || '#' ;
  @Input() review = null || 'review';
  @Input() numReview = null || 'numReview';


  faStar = faStar;
  
  constructor() { }

  ngOnInit() {
 
  }

}
