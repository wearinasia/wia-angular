import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CardBecomeHostComponent } from './card-become-host.component';

describe('CardBecomeHostComponent', () => {
  let component: CardBecomeHostComponent;
  let fixture: ComponentFixture<CardBecomeHostComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CardBecomeHostComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CardBecomeHostComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
