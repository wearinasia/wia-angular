import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-card-experience-booking-summary',
  templateUrl: './card-experience-booking-summary.component.html',
  styleUrls: ['./card-experience-booking-summary.component.css']
})
export class CardExperienceBookingSummaryComponent implements OnInit {
  @Input() bookdate = null || 'bookdate';
  @Input() guest = null || 'guest';
  @Input() rules = null || 'rules';

  constructor() { }

  ngOnInit() {
  }

}
