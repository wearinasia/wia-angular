import { Component, OnInit, Input } from '@angular/core';
import { faStar } from '@fortawesome/free-solid-svg-icons';


@Component({
  selector: 'app-card-experience-customer-review',
  templateUrl: './card-experience-customer-review.component.html',
  styleUrls: ['./card-experience-customer-review.component.css']
})

export class CardExperienceCustomerReviewComponent implements OnInit {
  faStar = faStar
  
  @Input() title = null || 'name'
  @Input() description = null || 'description'
  @Input() time = null || 'time'
  @Input() value = null || 'value'
  @Input() avatar = null || 'https://via.placeholder.com/500x500'

  constructor() { }

  ngOnInit() {
    
  }

}

