import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-card-experience-location',
  templateUrl: './card-experience-location.component.html',
  styleUrls: ['./card-experience-location.component.css']
})
export class CardExperienceLocationComponent implements OnInit {
  @Input() id = null
  @Input() name = null || 'name'
  @Input() image = null || 'image'

  constructor() { }

  ngOnInit() {
    
  }

}
