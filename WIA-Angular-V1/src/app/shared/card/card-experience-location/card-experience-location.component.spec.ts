import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CardExperienceLocationComponent } from './card-experience-location.component';

describe('CardExperienceLocationComponent', () => {
  let component: CardExperienceLocationComponent;
  let fixture: ComponentFixture<CardExperienceLocationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CardExperienceLocationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CardExperienceLocationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
