import { Component, OnInit } from '@angular/core';
import { MAT_DATE_LOCALE } from '@angular/material/core';
import { FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms';
import { Booking } from 'src/app/models/booking';
import { Product } from 'src/app/models/product';
import { Router } from '@angular/router';

@Component({
  selector: 'app-card-experience-booking-contact-info',
  templateUrl: './card-experience-booking-contact-info.component.html',
  styleUrls: ['./card-experience-booking-contact-info.component.css'],
  providers: [
    {provide: MAT_DATE_LOCALE, useValue: 'id'},
  ]
})
export class CardExperienceBookingContactInfoComponent implements OnInit {
  bookingSession: Booking;
  cartSession: Product;
  step: number;
  serviceType: string;
  bookingForm: FormGroup;
  private customerSession = JSON.parse(localStorage.getItem("customerSession"));

  idToken: string;
  bookdate: string;
  qty: string;
  name: any;
  price: any;

  constructor(private fb: FormBuilder, private router: Router) { 
    this.initializeForm();
    if(localStorage.getItem("bookingSession") == null ){
      this.bookingForm.patchValue(this.bookingForm);
    }else{
      const latestForm = JSON.parse(localStorage.getItem("bookingSession"));
      this.bookingForm.setValue(latestForm);
    }
  }

  ngOnInit() {
    if(this.customerSession == null){
      this.idToken = null;
    }else{
      this.customerSession = JSON.parse(localStorage.getItem("customerSession"));
      this.idToken = this.customerSession.token;
      console.log(this.idToken);
    }
    const details = JSON.parse(localStorage.getItem("bookingSession"));
    
    const cart = JSON.parse(localStorage.getItem("cartSession"));

    this.name = cart.name;
    this.price = cart.price;
    this.bookdate = cart.bookdate;
    this.qty = cart.qty;
  }

  submit(){
    this.bookingSession = this.bookingForm.value;
    localStorage.setItem('bookingSession', JSON.stringify(this.bookingSession));
    // localStorage.setItem('bookingSession', JSON.stringify({
    //   product: this.product,
    //   qty: this.qty,
    //   phone: this.phone,
    //   note: this.note,
    //   bookdate: this.bookdate,
    //   coupon: this.coupon,
    //   guest: {
    //     name: this.name,
    //     email: this.email,
    //     phone: this.phone,
    //     note: this.note
    //   }
    // }));
    this.router.navigate(['/booking-details'], {queryParams: {step: 2}});
  }

  initializeForm(){
    this.bookingForm = new FormGroup({
      name: new FormControl('', [Validators.required]),
      email: new FormControl('', [Validators.required]),
      phone: new FormControl('', [Validators.required]),
      note: new FormControl('', [Validators.required]),
    })
  }

}
