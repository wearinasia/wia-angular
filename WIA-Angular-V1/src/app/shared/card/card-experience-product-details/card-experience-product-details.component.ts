import { Component, OnInit, Input } from '@angular/core';
import { faStar } from '@fortawesome/free-solid-svg-icons';


@Component({
  selector: 'app-card-experience-product-details',
  templateUrl: './card-experience-product-details.component.html',
  styleUrls: ['./card-experience-product-details.component.css']
})
export class CardExperienceProductDetailsComponent implements OnInit {
  @Input() label = null || 'label' ;
  @Input() title = null || 'title' ;
  @Input() price = null || 'price' ;
  @Input() productdescription = null || 'productdescription' ;
  @Input() shortDescription = null || 'shortDescription' ;
  @Input() image = null || '#' ;
  @Input() avatar = null || 'https://via.placeholder.com/100x100' ;
  @Input() url = null || '#' ;
  @Input() name = null || 'name' ;
  @Input() location = null || 'location' ;
  @Input() duration = null || 'duration' ;
  @Input() description = null || 'description' ;
  @Input() review = null || 'review';

  @Input() ratingSummary = null || 'ratingSummary' ;
  @Input() ratingCount = null || 'ratingCount' ;
  faStar = faStar


  constructor() { }

  ngOnInit() {
  }

}
