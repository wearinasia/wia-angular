import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-card-experience-booking-schedule',
  templateUrl: './card-experience-booking-schedule.component.html',
  styleUrls: ['./card-experience-booking-schedule.component.css']
})
export class CardExperienceBookingScheduleComponent implements OnInit {
  @Input() booking_date = null || 'booking_date';
  @Input() guest = null || 'guest';

  constructor() { }

  ngOnInit() {
  }

}
