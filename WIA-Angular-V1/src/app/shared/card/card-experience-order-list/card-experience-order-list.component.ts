import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-card-experience-order-list',
  templateUrl: './card-experience-order-list.component.html',
  styleUrls: ['./card-experience-order-list.component.css']
})
export class CardExperienceOrderListComponent implements OnInit {
  @Input() label = null || 'label';
  @Input() title = null || 'title';
  @Input() description = null || 'description';
  @Input() booking_id = null || 'booking_id';
  @Input() image = null || 'image';
  @Input() status= null || 'status';
  @Input() url=null || 'url';

  constructor() { }

  ngOnInit() {
  }

}
