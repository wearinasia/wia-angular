import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CardExperienceOrderListComponent } from './card-experience-order-list.component';

describe('CardExperienceOrderListComponent', () => {
  let component: CardExperienceOrderListComponent;
  let fixture: ComponentFixture<CardExperienceOrderListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CardExperienceOrderListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CardExperienceOrderListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
