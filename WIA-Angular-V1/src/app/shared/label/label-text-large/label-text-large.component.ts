import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-label-text-large',
  templateUrl: './label-text-large.component.html',
  styleUrls: ['./label-text-large.component.css']
})
export class LabelTextLargeComponent implements OnInit {
  @Input() title = null || 'title';
  @Input() desc = null || 'desc';

  constructor() { }

  ngOnInit() {
  }

}
