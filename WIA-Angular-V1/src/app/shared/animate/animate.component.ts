import { Component, OnInit, Input } from '@angular/core';
import { trigger, state, style, animate, transition } from '@angular/animations';

@Component({
  selector: 'app-animate',
  templateUrl: './animate.component.html',
  styleUrls: ['./animate.component.css'],
  animations: [
    trigger('changeState', [
      state('state1', style({
        backgroundColor: 'yellow',
        transform: 'scale(1)'
      })),
      state('state2', style({
        backgroundColor: 'black',
        transform: 'scale(1)'
      })),
      transition('*=>state1', animate('500ms')),
      transition('*=>state2', animate('1000ms'))
    ])
  ]
})

export class AnimateComponent implements OnInit {
  @Input() currentState;
  constructor() { }
  ngOnInit() {
  }
}