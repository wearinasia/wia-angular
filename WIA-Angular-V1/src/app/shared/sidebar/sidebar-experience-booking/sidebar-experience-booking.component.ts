import { Component, OnInit, Input } from '@angular/core';
import { BookingService } from 'src/app/service/booking.service';
import { MAT_DATE_LOCALE } from '@angular/material/core';

@Component({
  selector: 'app-sidebar-experience-booking',
  templateUrl: './sidebar-experience-booking.component.html',
  styleUrls: ['./sidebar-experience-booking.component.css'],
  providers: [
    {provide: MAT_DATE_LOCALE, useValue: 'id'},
  ]
})
export class SidebarExperienceBookingComponent implements OnInit {
  @Input() title = null || 'title';
  @Input() price = null || 'price';
  @Input() booking_fee = null || 'booking_fee';
  @Input() promo = null || 'promo';
  @Input() payment_fee = null || 'payment_fee';
  @Input() total = null || 'total';
  coupon_code: string;
  discount_amount: string;

  constructor(private bookingService: BookingService) { }

  ngOnInit() {
  }

  applyVoucher(){
    this.bookingService.checkVoucher(this.coupon_code)
    .subscribe(discount => {
      console.log(this.coupon_code)
      var cartSession = JSON.parse(localStorage.getItem('cartSession'))
      cartSession.coupon = this.coupon_code
      cartSession.discount_amount = discount.discount_amount
      localStorage.setItem('cartSession',JSON.stringify(cartSession))
    })
  }
}
