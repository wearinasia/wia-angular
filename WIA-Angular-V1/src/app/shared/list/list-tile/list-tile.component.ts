import { Component, OnInit, Input } from '@angular/core';
import { faChevronRight } from '@fortawesome/free-solid-svg-icons';


@Component({
  selector: 'app-list-tile',
  templateUrl: './list-tile.component.html',
  styleUrls: ['./list-tile.component.css']
})
export class ListTileComponent implements OnInit {
  @Input() title =  null || 'title';
  @Input() subtitle = null;
  @Input() url = null || 'url';
  icon = faChevronRight
  constructor() { }

  ngOnInit() {
  }

}
