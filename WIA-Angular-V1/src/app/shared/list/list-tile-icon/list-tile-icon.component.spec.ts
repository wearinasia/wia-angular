import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListTileIconComponent } from './list-tile-icon.component';

describe('ListTileIconComponent', () => {
  let component: ListTileIconComponent;
  let fixture: ComponentFixture<ListTileIconComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListTileIconComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListTileIconComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
