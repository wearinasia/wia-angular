import { Component, OnInit, Input } from '@angular/core';
import { faCircle } from '@fortawesome/free-solid-svg-icons';


@Component({
  selector: 'app-list-payout',
  templateUrl: './list-payout.component.html',
  styleUrls: ['./list-payout.component.css']
})
export class ListPayoutComponent implements OnInit {
  icon =  faCircle;
  @Input() label = null || 'label';
  @Input() title = null || 'title';
  @Input() url = null || '#';
  @Input() time = null || 'time';
  @Input() price : any = null || 'price';
  @Input() subtitle = null || 'subtitle';
  @Input() status : any = null || 'status';
  //status : any;
  
 
  constructor() { }

  ngOnInit() {
  }

  ngClass(){
    if (this.status == 'complete'){
      return 'success'
    }
    if (this.status == 'processing_checkout'){
      return 'alert'
    }
    if (this.status == 'complete_payout_request'){
      return 'secondary'
    }

    
    
  }

}
