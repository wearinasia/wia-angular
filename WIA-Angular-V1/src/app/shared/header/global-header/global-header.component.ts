import { Component, OnInit, Input } from '@angular/core';
import { UserService } from 'src/app/service/user.service';
import { User } from 'src/app/models/user';
import { Location } from '@angular/common';
import { Title } from '@angular/platform-browser';
import { faArrowLeft } from '@fortawesome/free-solid-svg-icons';
import { faUser } from '@fortawesome/free-solid-svg-icons';
import { AuthService } from 'src/app/service/auth.service';
import { ActivatedRoute, Route, Router } from '@angular/router';
import { createSnackBarConfig } from 'src/app/models/snackbar';
import { MatSnackBar } from '@angular/material';


@Component({
  selector: 'app-global-header',
  templateUrl: './global-header.component.html',
  styleUrls: ['./global-header.component.css']
})
export class GlobalHeaderComponent implements OnInit {
  user: User
  isLoggedIn: Boolean
  @Input() isTransparent: Boolean
  faArrowLeft = faArrowLeft
  faUser = faUser
 
  
  constructor(
    private userService: UserService,
     private location: Location, 
     public pageTitle: Title,
     private authSvc: AuthService,
     private router: Router,
     private snackBar: MatSnackBar) {}

  ngOnInit() {    
    var custSession = JSON.parse(localStorage.getItem("customerSession"))
    if(custSession != null){
      this.userService.getUserProfile().subscribe(response=>{
        this.user = response;
        this.user.photo=this.user.photo
        this.isLoggedIn=true
      })
    }else{
      this.isLoggedIn=false
    }
  }

  ngClass(){
    return {
      'transparant': this.isTransparent == true ? true: false,
    }
  }

  back(){
    this.location.back();
  }

  logout(){
    if(this.authSvc.logout()) {
      this.ngOnInit();
      this.snackBar.open("You have been logged out!",
      null,
        createSnackBarConfig('toast_success')
      );
      this.router.navigate(['/']);
    }
  }
}
