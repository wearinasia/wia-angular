import { Component, OnInit, Input } from '@angular/core';
import { trigger, state, style, transition, animate } from '@angular/animations';



@Component({
  selector: 'app-modal',
  animations: [
    trigger('{{modalName}}', [
      // ...
      state('open', style({
        // height: '200px',
        opacity: 1,
        top:0,
      
      })),
      state('closed', style({
        // height: '100px',
        opacity: 0,
        height: 0,
        
      })),
      transition('open => closed', [
        animate(100)
      ]),
      transition('closed => open', [
        animate(100)
      ]),
    ]),
  ],
  templateUrl: './modal.component.html',
  styleUrls: ['./modal.component.css']
})


export class ModalComponent implements OnInit {
  @Input() modalName = null || 'title';
  constructor() { }

  ngOnInit() {
  }

  isOpen = false;

  toggle() {
    this.isOpen = !this.isOpen;
  }

}
