import { Component, ViewEncapsulation, ViewChild, OnInit } from '@angular/core';
import { CalendarComponent } from '@syncfusion/ej2-angular-calendars';
import { ActivatedRoute, Router } from '@angular/router';

import { CatalogService } from '../../service/catalog.service';
import { Product } from '../../models/product';
import { FormArray, FormControl, FormGroup } from '@angular/forms';
import { MatSnackBar } from '@angular/material';
import { createSnackBarConfig } from 'src/app/models/snackbar';

@Component({
  selector: 'app-calendar',
  templateUrl: './calendar.component.html',
  styleUrls: ['./calendar.component.css'],
  encapsulation: ViewEncapsulation.None
})

export class CalendarrComponent implements OnInit{
  product: Product;
  private customerSession = JSON.parse(localStorage.getItem("customerSession"));
  token: string = this.customerSession.token;
  
  public minDate: Date = new Date ();
  public year: number = new Date().getFullYear();
  public month: number = new Date().getMonth();
  public multiSelection: boolean = true;
  public dateValues: Array<Date> = [];

  @ViewChild('calendar', {static: false})

  public calendarObj: CalendarComponent;
  public dates: Date[] = [new Date()];

  globalProfileForm = new FormGroup({
    operationalDays: new FormGroup({
      weekDays: new FormArray([])
    })
  })

  constructor (
    private route: ActivatedRoute,
    private productService: CatalogService,
    private router: Router,
    private snackBar: MatSnackBar,
  ) {this.initializeGlobalProfileForm()}

  ngOnInit(){
    this.getProduct();
  }

  getProduct():void{
    const id = +this.route.snapshot.paramMap.get('id');
    this.productService.editProductById(id)
    .subscribe(product => {
      this.product = product;
      console.log(this.product.exp_available_date)
      console.log(this.product);
      // this.Date.patchValue(this.product.exp_available_date);

      this.product.exp_available_date.forEach((i)=>{
        var item = new Date(i);
        this.dateValues.push(item);
        console.log(this.product.exp_available_date)
      });
    });
  }

  initializeGlobalProfileForm(){
    this.globalProfileForm = new FormGroup({
        exp_available_date: new FormArray([])
    })
  }

  onValueChange(event): void {
    for(let i: number = 0; i < this.calendarObj.values.length; i++){
      if(this.Date.value.find(day => this.Date.value)){
        this.removeDays(i);
      }
      this.setDays(i, this.calendarObj.values[i].toLocaleDateString("fr-CA"))
    }
  }

  get Date(){
    return this.globalProfileForm.get('exp_available_date') as FormArray;
  }

  setDays(i, items){
    this.Date.setControl(i, new FormControl(items));
  }

  removeDays(i){
    this.Date.removeAt(i);
  }

  submit(){
    this.productService.updateProduct(this.globalProfileForm.value, this.product.id)
    .subscribe(response=>{
      if(response.hasOwnProperty('message_dialog')){
  
      }else{
        this.snackBar.open("Schedule has been updated successfully! Please wait...",
          null,
          createSnackBarConfig('toast_success', 3000)
        );
        setTimeout(()=>{
          location.href="account/customer/manage-experience/my-experience";
        }, 3000)
      }
      }
    );
  }
}
