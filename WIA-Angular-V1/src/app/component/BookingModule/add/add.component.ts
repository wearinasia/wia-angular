import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-add',
  templateUrl: './add.component.html',
  styleUrls: ['./add.component.css']
})
export class AddComponent implements OnInit {

  static step: number;
  
  private cartSession: {}
  private bookingSession: {}

  constructor(private route: ActivatedRoute) { }

  ngOnInit() {
    this.cartSession = JSON.parse(localStorage.getItem('cartSession'))
    this.bookingSession = JSON.parse(localStorage.getItem('bookingSession'))
  }

  static changeStep(s){
    AddComponent.step=s;
  }

  getStep(){
    let step = this.route.snapshot.queryParamMap.get("step")
    return step;
  }

}
