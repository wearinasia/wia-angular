import { Component, OnInit } from '@angular/core';
import { MAT_DATE_LOCALE } from '@angular/material/core';
import { FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { Booking } from '../../../../../models/booking';
import { Product } from 'src/app/models/product';
import { Title } from '@angular/platform-browser';

@Component({
  selector: 'app-booking-details',
  templateUrl: './booking-details.component.html',
  styleUrls: ['./booking-details.component.css'],
  providers: [
    {provide: MAT_DATE_LOCALE, useValue: 'id'},
  ]
})
export class BookingDetailsComponent implements OnInit {
  bookingSession: Booking;
  cartSession: Product;
  step: number;
  serviceType: string;
  bookingForm: FormGroup;
  private customerSession = JSON.parse(localStorage.getItem("customerSession"));

  idToken: string;
  bookdate: string;
  qty: string;
  name: any;
  price: any;
  discount_amount: any;

  payment_fee: any;
  total: any;

  constructor(private pageTitle: Title, private fb: FormBuilder, private router: Router) { 
    this.initializeForm();
    if(localStorage.getItem("bookingSession") == null ){
      this.bookingForm.patchValue(this.bookingForm);
    }else{
      const latestForm = JSON.parse(localStorage.getItem("bookingSession"));
      this.bookingForm.setValue(latestForm);
    }
  }

  ngOnInit() {
    this.pageTitle.setTitle('Booking Details');
    if(this.customerSession == null){
      this.idToken = null;
    }else{
      this.customerSession = JSON.parse(localStorage.getItem("customerSession"));
      this.idToken = this.customerSession.token;
      console.log(this.idToken);
    }
    
    const cart = JSON.parse(localStorage.getItem("cartSession"));
    
    if(cart == null) {
      this.router.navigateByUrl('/');
      return
    }

    this.name = cart.name;
    this.price = cart.price;
    this.bookdate = cart.bookdate;
    this.qty = cart.qty;
    this.discount_amount = cart.discount_amount;
    this.payment_fee = cart.qty * cart.price;
    this.total = (cart.qty * cart.price) - cart.discount_amount;
  }

  submit(){
    this.bookingSession = this.bookingForm.value;
    localStorage.setItem('bookingSession', JSON.stringify(this.bookingSession));
    this.router.navigate(['/booking-details'], {queryParams: {step: 2}});
  }

  initializeForm(){
    this.bookingForm = new FormGroup({
      name: new FormControl('', [Validators.required]),
      email: new FormControl('', [Validators.required]),
      phone: new FormControl('', [Validators.required]),
      note: new FormControl('', [Validators.required]),
    })
  }
}
