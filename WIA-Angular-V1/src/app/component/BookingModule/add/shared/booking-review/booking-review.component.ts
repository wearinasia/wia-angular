import {
  Component,
  OnInit
} from '@angular/core';
import {
  BookingService
} from 'src/app/service/booking.service';
import {
  Router
} from '@angular/router';
import { Booking } from 'src/app/models/booking';
import { Title } from '@angular/platform-browser';

@Component({
  selector: 'app-booking-review',
  templateUrl: './booking-review.component.html',
  styleUrls: ['./booking-review.component.css']
})
export class BookingReviewComponent implements OnInit {
  booking : Booking;
  name: string;
  email: string;
  phone: string;
  note: string;
  bookdate: string;
  qty: string;
  price: string;
  product: string;
  formName: string;
  coupon: string;
  bank: string;
  discount_amount: any;

  payment_fee: any;
  total: any;

  constructor(private pageTitle: Title, private bookingService: BookingService, private router: Router) {}

  ngOnInit() {
    this.pageTitle.setTitle('Booking Review');
    const details = JSON.parse(localStorage.getItem("bookingSession"));
    const cart = JSON.parse(localStorage.getItem("cartSession"))

    if(details == null) {
      this.router.navigateByUrl('/booking-details?step=1');
      return
    }

    this.name = details.name;
    this.email = details.email;
    this.phone = details.phone;
    this.note = details.note;

    this.name = cart.name;
    this.price = cart.price;
    this.product = cart.product;
    this.bookdate = cart.bookdate;
    this.qty = cart.qty;
    this.coupon = cart.coupon;
    this.discount_amount = cart.discount_amount;
    this.total = (cart.qty * cart.price) - cart.discount_amount;
    this.payment_fee = cart.qty * cart.price;
  }

  submit(): void {
    localStorage.setItem("lastBookingSession", JSON.stringify({
      product: this.product,
      qty: this.qty,
      phone: this.phone,
      note: this.note,
      bookdate: this.bookdate,
      coupon: this.coupon,
      guest: {
        name: this.name,
        email: this.email,
        phone: this.phone,
        note: this.note
      }
    }))
    const form = localStorage.getItem("lastBookingSession")
    this.bookingService.addBooking(form)
      .subscribe(booking => {
        if(booking.id != null){
          this.router.navigate(['/account/customer/booking/view/id/' + booking.id]);
        }
        else{
          this.router.navigate(['/']);
        }   
        localStorage.removeItem('cartSession');
        localStorage.removeItem('bookingSession');
        localStorage.removeItem('lastBookingSession');
        
        // console.log(booking);
        // console.log(booking.id);

        // this.bookingService.getBookingDetail(+booking.id, this.customerSession.token)
        //   .subscribe(bookingDetails => {
        //     console.log(bookingDetails);
        //     localStorage.setItem("paymentSession", JSON.stringify({
        //       payment_type: "bank_transfer",
        //       bank_transfer: {
        //         bank: this.bank == 'lain' ? 'permata' : 'permata'
        //       },
        //       transaction_details: {
        //         order_id: bookingDetails.id,
        //         gross_amount: +bookingDetails.grand_total,
        //       },
        //       customer_details: {
        //         email: bookingDetails.customer_email,
        //         first_name: bookingDetails.customer_name,
        //         phone: bookingDetails.phone,
        //       },
        //       item_details: {
        //         id: bookingDetails.product_id,
        //         price: (+bookingDetails.base_grand_total - +bookingDetails.voucher) / +bookingDetails.qty,
        //         quantity: +bookingDetails.qty,
        //         name: bookingDetails.name
        //       }
        //     }));
        //   })


        // this.bookingService.payment(form)
        //   .subscribe(booking => {
        //     var va, bankName;

        //     booking = booking;
        //     if (booking.hasOwnProperty["permata_va_number"]) {
        //       bankName = "permata"
        //       va = booking.permata_va_number
        //     } else {
        //       bankName = booking.va_numbers[0].bank
        //       va = booking.va_numbers[0].va_number
        //     }

        //     var paymentSession = JSON.parse(form);
        //     paymentSession.bankName = bankName;
        //     paymentSession.va = va;
        //     localStorage.setItem("paymentSession", JSON.stringify(paymentSession));

               //   })
      })
    //this.router.navigate(['/booking-details'], {queryParams: {step: 3}});
  }

}
