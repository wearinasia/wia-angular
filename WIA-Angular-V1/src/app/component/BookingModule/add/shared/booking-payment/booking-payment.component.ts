import { Component, OnInit } from '@angular/core';
import { BookingService } from 'src/app/service/booking.service';
import { delay } from 'q';

@Component({
  selector: 'app-booking-payment',
  templateUrl: './booking-payment.component.html',
  styleUrls: ['./booking-payment.component.css']
})
export class BookingPaymentComponent implements OnInit {
  order_id: string;
  gross_amount: string;
  email: string;
  first_name: string;
  phone: string;
  id: string;
  price: string;
  quantity: string;
  name: string;
  total: string;
  bookdate: string;
  grand_total: string;
  customer_email: string;
  customer_name: string;
  product_id: string;
  private customerSession = JSON.parse(localStorage.getItem("customerSession"));
  guest_name: string;
  qty: number;
  guest_phone: string;
  guest_email: string;
  note: string;
  token: string = this.customerSession.token;

  constructor(private bookingService: BookingService) { }

  async ngOnInit() {
    await delay(5);
    const form = JSON.parse(localStorage.getItem("formSession"));
    console.log(form);

    this.order_id = form.id
    this.gross_amount = form.grand_total

    this.email = form.customer_email
    this.first_name = form.customer_name
    this.phone = form.phone

    this.id = form.product_id
    this.price = form.price
    this.quantity = form.qty
    this.name = form.name

    this.total = form.base_grand_total
    this.bookdate = form.booking_date

    localStorage.setItem("paymentSession", JSON.stringify({
      payment_type: "bank_transfer",
      bank_transfer: {
        bank: "permata"
      },
      transaction_details: {
        order_id: this.order_id,
        gross_amount: this.gross_amount,
      },
      customer_details:{
        email: this.email,
        first_name: this.first_name,
        phone: this.phone,
      },
      item_details:{
        id: this.id,
        price: this.price,
        quantity: this.quantity,
        name: this.name
      } 
    }
    ))
  }
}
