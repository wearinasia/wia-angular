import { Component, OnInit } from '@angular/core';
import { AuthService } from '../../../service/auth.service';
import { Customer } from '../../../models/customer';
import { Router, ActivatedRoute } from '@angular/router';
import { FormGroup,FormControl, Validators } from '@angular/forms';
import { Auth } from 'src/app/models/auth';
import { Title } from '@angular/platform-browser';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {

  registerForm = new FormGroup({
    firstname: new FormControl('', Validators.required),
    lastname: new FormControl('', Validators.required),
    email: new FormControl('', Validators.required),
    password: new FormControl('', Validators.required),
  });
  auth: Auth;
  customer: Customer;

  hide = true;
  returnUrl: any;

  constructor(private pageTitle: Title, private authService: AuthService, private router: Router, private route: ActivatedRoute) { }

  ngOnInit() {
    this.pageTitle.setTitle('Register');
    this.returnUrl = this.route.snapshot.queryParams['returnUrl'] || '/';
  }

  onSubmit(){
    //console.warn(this.loginForm.value);
    this.authService.register(this.registerForm.value)
    .subscribe(
      auth =>{
        console.log(auth)
        if(auth.token != null){
          localStorage.setItem('customerSession',JSON.stringify(auth));
          this.router.navigateByUrl(this.returnUrl);
        }
        else{
          alert(auth.message_code);
        }
        
      }
    )
  }

}
