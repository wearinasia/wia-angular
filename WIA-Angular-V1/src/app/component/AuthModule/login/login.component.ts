import { Component, OnInit } from '@angular/core';
import { AuthService } from '../../../service/auth.service';
import { Customer } from '../../../models/customer';
import { Router, ActivatedRoute } from '@angular/router';
import { FormGroup,FormControl, Validators } from '@angular/forms';
import { Auth } from 'src/app/models/auth';
import { Title } from '@angular/platform-browser';
import { MatSnackBar } from '@angular/material';
import { createSnackBarConfig } from 'src/app/models/snackbar';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  loginForm = new FormGroup({
    email: new FormControl('', Validators.required),
    password: new FormControl('', Validators.required),
  });
  
  customerSession = JSON.parse(localStorage.getItem("customerSession"));

  auth: Auth;
  customer: Customer;
  returnUrl: any;
  step: Number;

  constructor(private pageTitle: Title, private authService: AuthService, 
    private router: Router, private route: ActivatedRoute,
    private snackBar: MatSnackBar){ }

  ngOnInit() {
    this.pageTitle.setTitle('Login');
    this.returnUrl = this.route.snapshot.queryParams['returnUrl'] || '/';
  }

  onSubmit(){
    this.route.queryParams.subscribe(params => {
      this.step = Number(params.step);
    });

    this.authService.login(this.loginForm.value)
    .subscribe(auth => {
      if(auth.message_code == '403'){
        //error 403 salah kombinasi!
        this.snackBar.open("Wrong email and/or password!",
            null,
            createSnackBarConfig('toast_danger')
          );
      }else{
        this.snackBar.open("Logged in successfully!",
            null,
            createSnackBarConfig('toast_success')
          );
        this.authService.setCustomerSession(auth);
        if(this.step != 1){
          this.router.navigateByUrl('home');
        }
        else if(this.step == 1){
          location.reload();
        }
      }
    })
  }
}
