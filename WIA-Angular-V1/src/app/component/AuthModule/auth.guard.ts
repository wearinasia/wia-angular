import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { AuthService } from 'src/app/service/auth.service';
import { UserService } from 'src/app/service/user.service';
import { HttpErrorResponse } from '@angular/common/http';

@Injectable({
    providedIn: 'root'
})

export class AuthGuard implements CanActivate {
    constructor(
        private router: Router,
        private authService: AuthService,
        private userService: UserService){}
    step: number;
    appidToken: string;

    canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {

        if(this.authService.getLoggedIn() != false){
            return this.userService.getUserProfile().pipe(
                map((auth) => {
                    if(auth){
                        return true;
                    }
                    this.router.navigateByUrl('/account/login');
                    return false;
                },
                (error: HttpErrorResponse) => {
                    this.router.navigateByUrl('/account/login');
                    return false;
                })
            )} 
            else {
            this.router.navigateByUrl('/account/login');
            return false;
        }
    }
}