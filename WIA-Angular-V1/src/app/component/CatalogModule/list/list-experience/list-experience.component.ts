import { Component, OnInit, Input } from '@angular/core';
import { Product } from '../../../../models/product';
import { CatalogService } from '../../../../service/catalog.service';

@Component({
  selector: 'app-list-experience',
  templateUrl: './list-experience.component.html',
  styleUrls: ['./list-experience.component.css']
})
export class ListExperienceComponent implements OnInit {
  @Input() Product:Product;
  @Input() ProductService:CatalogService;
  @Input() expLists: any;
  @Input() exp: any;

  constructor() { }

  ngOnInit() {
  }

}
