import { Component, OnInit } from '@angular/core';
import { Category } from 'src/app/models/category';
import { ActivatedRoute, Router } from '@angular/router';
import { Title } from '@angular/platform-browser';
import { CatalogService } from 'src/app/service/catalog.service';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.css']
})
export class ListComponent implements OnInit {
  category: Category;
  category_id: number;

  constructor(
    private productService: CatalogService,
    private route: ActivatedRoute,
    private router: Router,
    private pageTitle: Title
    ) { }

  ngOnInit() {
    
    this.getProductList()
  }


  getProductList(){
    const id = +this.route.snapshot.paramMap.get('id');
    this.productService.getProductsByCategoryId(id)
    .subscribe(category => {
      this.category = category;
      this.pageTitle.setTitle(category.category_name);
     //console.log(category);
    });
  }

  getReview


  go(button_id):void{
    this.router.navigateByUrl('catalog/category/id/' + button_id);
  }
}
