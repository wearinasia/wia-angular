import { Component, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { CatalogService } from '../../../service/catalog.service';
import { Router } from '@angular/router';
import { Title } from '@angular/platform-browser';

import { Product } from '../../../models/product';
import { Booking } from '../../../models/booking';
import { FormGroup, FormControl, Validators, FormBuilder, FormArray } from '@angular/forms';
import { BookingService } from 'src/app/service/booking.service';
import { CalendarComponent } from '@syncfusion/ej2-angular-calendars';
import { ReviewService } from 'src/app/service/review.service';
import { Review } from 'src/app/models/review';

@Component({
  selector: 'app-view',
  templateUrl: './view.component.html',
  styleUrls: ['./view.component.css'],
})

export class ViewComponent implements OnInit {
  product: Product;
  booking: Booking;
  numbers: number[];
  bookingForm: FormGroup;
  cartSession: any;
  bookdate: any;
  qty: any;
  product_id: number;
  name: string;
  bio: string;

  public minDate: Date = new Date();
  @ViewChild('calendar', {
    static: false
  })
  public calendarObj: CalendarComponent;
  public minTime: Date = new Date('12:00 AM');
  public maxTime: Date = new Date('12:00 PM');
  reviews: Review;

  constructor(
    private fb: FormBuilder,
    private route: ActivatedRoute,
    private router: Router,
    private productService: CatalogService,
    private bookingService: BookingService,
    private pageTitle: Title,
    private reviewService: ReviewService,
  ) {
    this.initializeForm();

  }

  ngOnInit() {
    
    this.getProducts();
    this.getProductReview();
  }

  getProducts(): void {
    const id = +this.route.snapshot.paramMap.get('id');
    this.productService.getProductById(id)
      .subscribe(product => {
        this.product = product;
        this.pageTitle.setTitle(product.name);
        this.product.exp_include = this.product.exp_include.filter(function (item) {
          return (item.value != "" && item.value != null);
        });
        for (let i = 0; i < this.product.exp_available_date.length; i++) {
          this.setDate(this.product.exp_available_date[i]);
        }
        for (let i = this.bookDate.length; i != -1; i--) {
          this.removeDate(i);
        }

        var bucket = [];
        var n = parseInt(product.tetapanqty);
        for (var a = 1; a <= n; a++) {
          bucket.push(a);
          this.numbers = bucket;
        }
        // this.bookingForm.get('bookdate').setValue(this.product.exp_available_date[0]);
      });
  }

  getProductReview() {
    const id = +this.route.snapshot.paramMap.get('id');
    this.reviewService.getReview(id)
      .subscribe(Response => {
        this.reviews = Response;
        console.log(Response);

      });
  }

  checkAvailability(event) {
    this.addDate(0, event.value.toLocaleDateString("fr-CA"));
    this.bookingService.checkAvailability(this.product.id, event.value.toLocaleDateString("fr-CA"))
      .subscribe(booking => {
        this.booking = booking

        var bucket = [];
        var n = parseInt(this.booking.availability);
        this.numbers = [];
        for (var a = 1; a <= n; a++) {
          bucket.push(a);
          this.numbers = bucket;
        }
      })
  }

  onLoad(event) {
    event.isDisabled = true;
    for (let i = 0; i < this.product.exp_available_date.length; i++) {
      if (event.date.toLocaleDateString("fr-CA") === this.product.exp_available_date[i]) {
        event.isDisabled = false;
      }
    }
  }

  submit() {
    this.cartSession = this.bookingForm.value;
    localStorage.setItem('cartSession', JSON.stringify({
      price: this.product.price,
      name: this.product.name,
      product: this.product.id,
      bookdate: this.cartSession.bookdate[0],
      qty: this.cartSession.qty
    }));
    this.router.navigate(['/booking-details'], {
      queryParams: {
        step: 1
      }
    });
  }

  initializeForm() {
    this.bookingForm = new FormGroup({
      bookdate: new FormArray([]),
      qty: new FormControl('', [Validators.required]),
    })
  }

  get bookDate() {
    return this.bookingForm.get('bookdate') as FormArray;
  }

  removeDate(i) {
    this.bookDate.removeAt(i);
  }

  addDate(i, items) {
    this.bookDate.setControl(i, new FormControl(items));
  }

  setDate(items) {
    this.bookDate.push(new FormControl(items));
  }

}

