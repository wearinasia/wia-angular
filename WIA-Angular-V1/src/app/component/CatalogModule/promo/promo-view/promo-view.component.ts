import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { CatalogService } from 'src/app/service/catalog.service';
import { Category } from 'src/app/models/category';
import { Title } from '@angular/platform-browser';

@Component({
  selector: 'app-promo-view',
  templateUrl: './promo-view.component.html',
  styleUrls: ['./promo-view.component.css']
})
export class PromoViewComponent implements OnInit {
  promoID: String
  promo: {};
  category : Category;
  constructor(private route: ActivatedRoute, private catalogService: CatalogService, private pageTitle: Title) { }

  ngOnInit() {
    this.pageTitle.setTitle('Promo');
    // this.promo = {
    //   "category_id": String,
    //   "category_name": String,
    //   "image": String,
    //   "label": String,
    //   "title": String,
    //   "short_description": String,
    //   "subcategory": String,
    //   "products": String
    // }
    this.getPromoDetails();
  }

  getPromoDetails(){
    this.promoID = this.route.snapshot.paramMap.get('id');
    this.catalogService.getPromoCategory(+this.promoID).subscribe(
      (response)=> {
        console.log(response)
        this.category = response
      }
    )
  }

}
