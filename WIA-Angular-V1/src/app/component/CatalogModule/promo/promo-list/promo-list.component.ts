import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { CatalogService } from 'src/app/service/catalog.service';

@Component({
  selector: 'app-promo-list',
  templateUrl: './promo-list.component.html',
  styleUrls: ['./promo-list.component.css']
})
export class PromoListComponent implements OnInit {
  id=714
  promos: []

  constructor(private route: ActivatedRoute, private catalogService: CatalogService) { }

  ngOnInit() {
    this.catalogService.getPromoCategory(+this.id).subscribe(
      (response)=> {
        this.promos = response.subcategory
        console.log(this.promos)
      }
    )
  }

}
