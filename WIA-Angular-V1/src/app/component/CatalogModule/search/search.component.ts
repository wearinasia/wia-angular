import { Component, OnInit } from '@angular/core';

import { faMapMarkerAlt } from '@fortawesome/free-solid-svg-icons';
import { faArrowCircleRight } from '@fortawesome/free-solid-svg-icons';
import { faShoppingBag } from '@fortawesome/free-solid-svg-icons';
import { faSearch } from '@fortawesome/free-solid-svg-icons';
import { FormControl } from '@angular/forms';
import { User } from 'src/app/models/user';
import { Title } from '@angular/platform-browser';
import { UserService } from 'src/app/service/user.service';
import { CatalogService } from 'src/app/service/catalog.service';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.css']
})
export class SearchComponent implements OnInit {

  faSearch = faSearch
  faLocationArrow = faMapMarkerAlt
  faArrowCircleRight = faArrowCircleRight

  faShoppingBag = faShoppingBag
  
  locationKey = 642;
  activityKey = 706;

  TYPE_LOCATION = 0;
  TYPE_ACTIVITY = 1;
  TYPE_PRODUCT = 3;

  ready: number

  matcher: string
  searchControl = new FormControl('');
  
  filteredSearchList: any[]
  searchList: any[]
  
  constructor(private pageTitle: Title, private userService: UserService, 
    private productService: CatalogService, private router: Router, 
    private route: ActivatedRoute ) { }

  ngOnInit() {
    this.searchList=[];
    this.ready=0;

    this.ready-=1;
    this.productService.getProductsByCategoryId(this.activityKey)
    .subscribe(activities => {

          var temp: any[];
          temp = activities.subcategory;
          temp.map(item => item.type=this.TYPE_ACTIVITY)
          temp.sort((a,b) => a.name.localeCompare(b.name));
          this.searchList.push(...temp);
          this.ready+=1
    })

    this.ready-=1;
    this.productService.getProductsByCategoryId(this.locationKey)
    .subscribe(locations => {
          var temp: any[];
          temp = locations.subcategory
          temp.map(item => item.type=this.TYPE_LOCATION)
          temp.sort((a,b) => a.name.localeCompare(b.name));
          this.searchList.push(...temp)
          this.ready+=1
    })
  }

  redirect(type,id){
    setTimeout(()=>{
      switch(type){
        case this.TYPE_LOCATION:{
          this.router.navigate(['/catalog/category/id/' + id])
          break;
        }
        case this.TYPE_ACTIVITY:{
          this.router.navigate(['/catalog/category/id/' + id])
          break;
        }
        case this.TYPE_PRODUCT:{
          this.router.navigate(['catalog/product/id/' + id])
          break;
        }
      }
    },800)
  }

  check(e){
    this.filteredSearchList = this.searchList.filter(item => item.name.toLowerCase().includes(e.toLowerCase()));
    if(this.filteredSearchList.length == 0) this.matcher = "ZeroResult"
    else this.matcher = "OK"
  }

}
