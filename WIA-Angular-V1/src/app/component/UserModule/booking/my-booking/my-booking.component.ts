import { Component, OnInit } from '@angular/core';
import { Booking } from '../../../../models/booking';
import { BookingService } from '../../../../service/booking.service';
import {labelStatusAsCustomer} from 'src/app/models/labels'
import { Title } from '@angular/platform-browser';

@Component({
  selector: 'app-my-booking',
  templateUrl: './my-booking.component.html',
  styleUrls: ['./my-booking.component.css']
})

export class MyBookingComponent implements OnInit {
  booking: Booking[];
  booking_id: string;
  orderDate: any[];

  constructor(private pageTitle: Title, private bookingService: BookingService) { }

  ngOnInit() {
    this.getHistory();
  }

  getHistory():void{
    this.pageTitle.setTitle('My Booking');
    this.bookingService.getHistoryList()
    .subscribe(booking => {
      var temp=[];
      for(let x in booking){
        for(let y in booking[x]) booking[x][y].label=labelStatusAsCustomer[booking[x][y].status]
        temp.push({
          'date': x,
          'orders': booking[x]
        })
      }
      this.orderDate = temp;
    });
  }

}
