import { Component, OnInit } from '@angular/core';
import { Booking } from 'src/app/models/booking';
import { BookingService } from 'src/app/service/booking.service';
import { ActivatedRoute } from '@angular/router';
import { orderStatus, labelStatusAsCustomer} from 'src/app/models/labels';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Title } from '@angular/platform-browser';

import { ReviewService } from '../../../../service/review.service'

@Component({
  selector: 'app-my-booking-view',
  templateUrl: './my-booking-view.component.html',
  styleUrls: ['./my-booking-view.component.css']
})
export class MyBookingViewComponent implements OnInit {
  booking: Booking;
  // ratings: string[] = ['1', '2', '3', '4', '5'];

  orderStatusCode: number;

  reviewForm = new FormGroup({
    title: new FormControl('', [
      Validators.required,
    ]),
    rating: new FormControl('', [
      // Validators.pattern(/^[0-9]+$/)
      Validators.max(5), Validators.min(1)
    ]),
    reviews: new FormControl('', [
      Validators.required,
    ]),
  });

  constructor(
    private bookingService: BookingService,
    private reviewService: ReviewService,
    private route: ActivatedRoute,
    private pageTitle: Title
  ) { }

  ngOnInit() {
    this.pageTitle.setTitle('My Booking Details');
    this.getBooking();
  }

  getBooking(): void{
    const booking_id = +this.route.snapshot.paramMap.get('id');
    this.bookingService.getBookingDetail(booking_id)
    .subscribe(booking => {
      console.log("Booking view")
      this.booking = booking
      console.log(this.booking)

      this.orderStatusCode = orderStatus[this.booking.status] // storing key state from orderStatus enumerations
      this.booking.status = labelStatusAsCustomer[this.booking.status] // storing label state based from labelStatus enumeration
          
      this.booking = booking
    })
  }

  submit(): void{
    this.reviewService.addReview(this.reviewForm.value, +this.booking.product_id)
    .subscribe(Response => {
      console.log(Response)
      console.log(+this.booking.product_id)
    }
    )
  }
}
