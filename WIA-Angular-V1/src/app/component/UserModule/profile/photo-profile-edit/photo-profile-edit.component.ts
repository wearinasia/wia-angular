import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { Title } from '@angular/platform-browser';
import { UserService } from 'src/app/service/user.service';
import { Router } from '@angular/router';
import { Compiler } from '@angular/core';

class ImageB64{
  image: String;
  type: String;
  base64: String
  constructor(image: String, type: String, value: String){
    this.image=image
    this.type=type
    this.base64=value
  }
  toJson(){
    return{
      'image': this.image,
      'type': this.type,
      'base64': this.base64
    }
  }
}

@Component({
  selector: 'app-photo-profile-edit',
  templateUrl: './photo-profile-edit.component.html',
  styleUrls: ['./photo-profile-edit.component.css'],
  encapsulation: ViewEncapsulation.None,
})
export class PhotoProfileEditComponent implements OnInit {
  user: any
  isNotUploading: boolean;

  photo:ImageB64

  constructor(private pageTitle: Title, private userService: UserService, private route: Router, private compiler: Compiler) { }

  ngOnInit() {
    this.pageTitle.setTitle('Change Profile Photo')
    this.user = JSON.parse(localStorage.getItem("customerSession"))
    this.isNotUploading = true;
  }

  onSelected(event){
    console.log(event);
    var file = event.target.files[0];
    if(file.size > 5000000){
      alert("Profile photo cannot exceed 5MB")
      return
    }
    const fileReader: FileReader = new FileReader();
    fileReader.readAsDataURL(file);
    fileReader.onload = (event: Event) => { 
      let value: any = fileReader.result as String
      value = value.split(',')
      value[0]=value[0].slice(value[0].indexOf(':')+1,value[0].indexOf(';'))
      this.photo = new ImageB64(this.user.customer_id,value[0],value[1]);
      console.log(this.photo);
    }
  }

  onUpload(){
    this.isNotUploading = false;
    this.userService.changeProfilePhoto(this.photo).subscribe((response)=>{
      if(response.message_code == 200){
        alert("Profile photo upload is successful!")
        this.compiler.clearCache()
        this.route.navigateByUrl('/account/customer/profile')
      }
    })
  }

}
