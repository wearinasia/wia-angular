import { Component, OnInit } from '@angular/core';
import { User } from 'src/app/models/user';
import { UserService } from 'src/app/service/user.service';
import { Title } from '@angular/platform-browser';


@Component({
  selector: 'app-my-profile',
  templateUrl: './my-profile.component.html',
  styleUrls: ['./my-profile.component.css']
})
export class MyProfileComponent implements OnInit {
  user: User;

  constructor(
    private userService: UserService,
    private pageTitle: Title
  ) { }

  ngOnInit() {
    this.pageTitle.setTitle('My Profile');
    this.getProfile();
  }

  getProfile(): void{
    this.userService.getUserProfile()
    .subscribe(user => {
      console.log(user);
      this.user = user
    })
  }
}
