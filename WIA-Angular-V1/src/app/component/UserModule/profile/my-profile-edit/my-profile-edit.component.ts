import { Component, OnInit } from '@angular/core';

import { ActivatedRoute, Router } from '@angular/router';
import { UserService } from '../../../../service/user.service';
import { User } from '../../../../models/user';
import { Title } from '@angular/platform-browser';
import { FormGroup, FormControl, Validators } from '@angular/forms';

@Component({
  selector: 'app-my-profile-edit',
  templateUrl: './my-profile-edit.component.html',
  styleUrls: ['./my-profile-edit.component.css']
})
export class MyProfileEditComponent implements OnInit {
  user: User;
  
  editProfile = new FormGroup({
    firstname: new FormControl('', Validators.required),
    email: new FormControl('', Validators.required),
    customer_job: new FormControl('', Validators.required),
    customer_bio: new FormControl('', Validators.required),
    address: new FormGroup({
      company: new FormControl('', Validators.required),
      city: new FormControl('', Validators.required),
      region: new FormControl('', Validators.required),
      postcode: new FormControl('', Validators.required),
      street: new FormControl('', Validators.required),
      telephone: new FormControl('', Validators.required)
    })
  })

  private customerSession = JSON.parse(localStorage.getItem("customerSession"));
  token: string = this.customerSession.token;

  constructor(
    private route: ActivatedRoute,
    private userService: UserService,
    private router: Router,
    private pageTitle: Title
  ) { }

  ngOnInit() {
    this.pageTitle.setTitle('Manage Profile');
    this.getProfile();
  }

  getProfile():void{
    this.userService.getUserProfile()
    .subscribe(user => {
      console.log(user)
      this.editProfile.patchValue(user);
    });
  }

  nextStep(): void{
    this.router.navigate(['account/customer/profile'])
  }

  onSubmit():void{
    this.userService.updateUserProfile(this.editProfile.value)
    .subscribe((response)=>{
      this.nextStep()}
      );
  }

}
