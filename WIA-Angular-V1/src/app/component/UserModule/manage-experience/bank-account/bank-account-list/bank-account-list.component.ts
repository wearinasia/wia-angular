import { Component, OnInit } from '@angular/core';
import { User } from '../../../../../models/user';
import { UserService } from '../../../../../service/user.service';
import { Title } from '@angular/platform-browser';
import { Router } from '@angular/router';

@Component({
  selector: 'app-bank-account-list',
  templateUrl: './bank-account-list.component.html',
  styleUrls: ['./bank-account-list.component.css']
})
export class BankAccountListComponent implements OnInit {
  user: User[];
  private customerSession = JSON.parse(localStorage.getItem("customerSession"));
  token: string = this.customerSession.token;
  bank: {};

  constructor(private router: Router, private pageTitle: Title, private userService: UserService) { }

  ngOnInit() {
    this.pageTitle.setTitle('Bank Account')
    this.getBankAccount();
  }

  getBankAccount():void{
    this.userService.getBank()
    .subscribe(bank => {
      this.bank = bank;
    });
  }
}
