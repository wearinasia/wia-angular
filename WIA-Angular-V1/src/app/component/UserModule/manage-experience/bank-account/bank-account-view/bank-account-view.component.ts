import { Component, OnInit } from '@angular/core';
import { User } from '../../../../../models/user';
import { UserService } from '../../.././../../service/user.service';
import { ActivatedRoute, Router } from '@angular/router';
import { FormGroup, FormControl, Validators, FormControlName } from '@angular/forms';

@Component({
  selector: 'app-bank-account-view',
  templateUrl: './bank-account-view.component.html',
  styleUrls: ['./bank-account-view.component.css']
})

export class BankAccountViewComponent implements OnInit {
  user: User;
  bank: {};
  bankID

  MODE_POST=0
  MODE_EDIT=1

  mode: number
  
  bankForm = new FormGroup({
    acc_name: new FormControl('', Validators.required),
    acc_number: new FormControl('', Validators.required),
    bank_name: new FormControl('', Validators.required),
  });

  constructor(
    private userService: UserService,
    private router: Router,
    private route: ActivatedRoute
  ) { }

  ngOnInit() {
    this.bank={};
    this.getBank();
  }

  getBank():void{
    this.bankID = this.route.snapshot.paramMap.get('id');
    if(this.bankID == null) this.mode = this.MODE_POST
    else this.mode = this.MODE_EDIT
    this.userService.getBank()
    .subscribe(banks => {
      for(let item of banks){
        if(item.acc_id === this.bankID) {
          this.bank = item;
          this.bankForm.patchValue(this.bank)
        }
        break;
      }
    })
  }

  nextStep(): void{
    this.router.navigate(['/account/customer/manage-experience/bank-account/list'])
  }

  onSubmit(){
    switch(this.mode){
      case(this.MODE_POST):{
        this.userService.addBank(this.bankForm.value)
        .subscribe(() => this.nextStep());
        break;
      }
      case(this.MODE_EDIT):{
        this.userService.editBank(this.bankForm.value, this.bankID)
        .subscribe(() => this.nextStep());
        break;
      }
    }
  }
}