import { Component, OnInit } from '@angular/core';
import { Booking } from 'src/app/models/booking';
import { BookingService } from 'src/app/service/booking.service';
import { ActivatedRoute } from '@angular/router';
import {orderStatus, labelPayout} from 'src/app/models/labels';
import { Title } from '@angular/platform-browser';

@Component({
  selector: 'app-payout-view',
  templateUrl: './payout-view.component.html',
  styleUrls: ['./payout-view.component.css']
})

export class PayoutViewComponent implements OnInit {
  booking: Booking;

  orderStatusCode: number;

  statusLabel: string;

  constructor(
    private pageTitle: Title,
    private bookingService: BookingService,
    private route: ActivatedRoute
  ) { }

  ngOnInit() {
    this.pageTitle.setTitle('Payout Request');
    this.getCheckOutOrder();
  }

  getCheckOutOrder(): void{
    const booking_id = +this.route.snapshot.paramMap.get('id');
    this.bookingService.getPayoutDetail(booking_id)
    .subscribe(booking => {     
      console.log("Order view")
      this.booking = booking
      this.orderStatusCode = orderStatus[this.booking.status] // storing key state from orderStatus enumerations

      this.statusLabel = labelPayout[this.booking.status] // storing label state based from labelStatus enumeration
    })
  }

  requestPayment(){
    switch(this.orderStatusCode){
      case orderStatus.processing_checkout:{
        this.bookingService.changeBookingStatus(this.booking.id, orderStatus[3]).subscribe(response=>{
          this.getCheckOutOrder()
        })
        break;
      }
    }
  }

}
