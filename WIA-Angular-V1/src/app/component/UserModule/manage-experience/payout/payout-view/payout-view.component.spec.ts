import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PayoutViewComponent } from './payout-view.component';

describe('PayoutViewComponent', () => {
  let component: PayoutViewComponent;
  let fixture: ComponentFixture<PayoutViewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PayoutViewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PayoutViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
