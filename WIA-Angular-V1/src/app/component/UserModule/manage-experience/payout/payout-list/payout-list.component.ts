import { Component, OnInit } from '@angular/core';
import { BookingService } from '../../../../../service/booking.service';
import { Title } from '@angular/platform-browser';
import { Payout } from '../../../../../models/payout';

@Component({
  selector: 'app-payout-list',
  templateUrl: './payout-list.component.html',
  styleUrls: ['./payout-list.component.css']
})

export class PayoutListComponent implements OnInit {
  payout: Payout;

  constructor(private bookingService: BookingService, private pageTitle: Title) { }

  ngOnInit() {
    this.pageTitle.setTitle('Payout Request')
    this.getHistory();
  }

  getHistory():void{
    this.bookingService.getPayout()
    .subscribe(payout => {
      this.payout = payout
    });
  }
}
