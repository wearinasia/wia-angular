import { Component, OnInit } from '@angular/core';
import { Booking } from '../../../../../models/booking';
import { BookingService } from '../../../../../service/booking.service';
import { labelStatusAsHost } from 'src/app/models/labels';
import { Title } from '@angular/platform-browser';

@Component({
  selector: 'app-my-orders',
  templateUrl: './my-orders.component.html',
  styleUrls: ['./my-orders.component.css']
})

export class MyOrdersComponent implements OnInit {
  booking: Booking[];
  booking_id: string;
  orderDate: any[];

  constructor(private bookingService: BookingService, private pageTitle: Title) { }

  ngOnInit() {
    this.pageTitle.setTitle('My Orders')
    this.getBooking();
  }

  getBooking():void{
    this.bookingService.getOrdersList()
    .subscribe(booking => {
      var temp=[];
      for(let x in booking){
        for(let y in booking[x]){
          booking[x][y].label = labelStatusAsHost[booking[x][y].status]
        }
        temp.push({
          'date': x,
          'orders': booking[x]
        })
      }
      this.orderDate = temp;
    });
  }

}
