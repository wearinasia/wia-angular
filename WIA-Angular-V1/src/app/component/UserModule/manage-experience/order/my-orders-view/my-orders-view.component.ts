import { Component, OnInit } from '@angular/core';
import { Booking } from 'src/app/models/booking';
import { BookingService } from 'src/app/service/booking.service';
import { ActivatedRoute } from '@angular/router';
import {orderStatus, labelStatusAsHost} from 'src/app/models/labels';
import { Title } from '@angular/platform-browser';

@Component({
  selector: 'app-my-orders-view',
  templateUrl: './my-orders-view.component.html',
  styleUrls: ['./my-orders-view.component.css']
})

export class MyOrdersViewComponent implements OnInit {
  booking: Booking;

  bankName: String;
  va: String;
  orderStatusCode: number;

  statusLabel: string;

  constructor(
    private pageTitle: Title,
    private bookingService: BookingService,
    private route: ActivatedRoute
  ) { }

  ngOnInit() {
    this.pageTitle.setTitle('My Orders')
    this.getBooking();
  }

  getBooking(): void{
    
    console.log(orderStatus[3])

    const booking_id = +this.route.snapshot.paramMap.get('id');
    this.bookingService.getOrderDetail(booking_id)
    .subscribe(booking => {     
      console.log("Order view")
      this.booking = booking
  
      this.orderStatusCode = orderStatus[this.booking.status] // storing key state from orderStatus enumerations
      this.booking.status = labelStatusAsHost[this.booking.status] // storing label state based from labelStatus enumeration
                  
      var paymentSession = JSON.parse(localStorage.getItem("paymentSession"));
      this.bankName = paymentSession.bankName;
      this.va = paymentSession.va;
    })
  }

  changeStatus(){
    switch(this.orderStatusCode){
      case orderStatus.processing:{
        this.bookingService.changeBookingStatus(this.booking.id, orderStatus[1]).subscribe(response=>{
          this.getBooking()
        })
        break;
      }
      case orderStatus.processing_checkin:{
        this.bookingService.changeBookingStatus(this.booking.id, orderStatus[2]).subscribe(response=>{
          this.getBooking()
        })
        break;
      }
    }
  }

}
