import { Component, Input, OnInit } from '@angular/core';
import { HttpClient, HttpEventType, HttpHeaders } from '@angular/common/http';

import { CatalogService } from '../../../../../../../service/catalog.service';
import { Product } from '../../../../../../../models/product';
import { Title } from '@angular/platform-browser';
import { Router } from '@angular/router';
import { MatSnackBar } from '@angular/material';
import { createSnackBarConfig } from 'src/app/models/snackbar';


const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

class ImageB64{
  image: String;
  type: String;
  base64: String
  constructor(image: String, type: String, base64: String){
    this.image=image
    this.type=type
    this.base64=base64
  }
}

@Component({
  selector: 'app-photo-upload',
  templateUrl: './photo-upload.component.html',
})

export class PhotoUploadComponent implements OnInit {
  @Input() product: Product;
    
  isUploading: Boolean;

  photos:ImageB64[]

  failedPhoto: String[];

  constructor(private router: Router, private http: HttpClient, 
    private productService: CatalogService, private pageTitle: Title,
    private snackBar: MatSnackBar) {
    this.photos=[]
    this.failedPhoto=[]
  }

  ngOnInit(){
    this.pageTitle.setTitle('Manage Experience')
    this.isUploading = false;
  }

  onUpload(){
    var send={
      id: "",
      products:[]
    };
    send.id = this.product.id.toString();
    send.products=this.photos;    
    this.isUploading = true;
    this.productService.uploadPhoto(send)
    .subscribe(response => {
        if(response.message_code == 200){
          this.snackBar.open("Photo has been uploaded successfully!",
          null,
          createSnackBarConfig('toast_success')
        );
        this.isUploading = false;
        this.router.navigate(['account/customer/manage-experience/my-experience/edit/id/'+this.product.id], {queryParams: {step: 6}} )
      }
    })
  }

  skip(){
    this.router.navigate(['account/customer/manage-experience/my-experience/edit/id/'+this.product.id], {queryParams: {step: 6}} )
  }

  onFileSelected(event){
    this.failedPhoto = []
    for (var i = 0; i < event.target.files.length; i++) {
      this.uploadSingleFile(event.target.files[i])
    }
  }

  deleteImage(index){
    this.photos.splice(index,1);
  }

  private uploadSingleFile(files){
    if(files.size > 5000000) {
      this.failedPhoto.push(files.name)
      return
    }
    const fileReader: FileReader = new FileReader();
    fileReader.readAsDataURL(files); // read file as data url
    fileReader.onload = (event: Event) => { // called once readAsDataURL is completed
      let value: any = fileReader.result as String

      value = value.split(',')
      value[0]=value[0].slice(value[0].indexOf(':')+1,value[0].indexOf(';'))
      
      this.photos.push(
        new ImageB64(this.product.id+'_'+this.photos.length,value[0],value[1])
      );
    }
  }
}