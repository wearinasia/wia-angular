import { Component, OnInit, Input } from '@angular/core';
import { Title } from '@angular/platform-browser';

@Component({
  selector: 'app-manage-schedule',
  templateUrl: './manage-schedule.component.html',
  styleUrls: ['./manage-schedule.component.css']
})
export class ManageScheduleComponent implements OnInit {
  @Input() product;

  constructor(
    private pageTitle: Title
  ) { }

  ngOnInit() {
    this.pageTitle.setTitle('Manage Experience')
  }
  // dateClass = (d: Date) => {
  //   this.datePipe.transform(this.date, 'yyyy-MM-dd');
  //   const date = d.getDate();
  //   return (date === 30) ? 'example-custom-date-class' : undefined;
  // }

  // check(){
  //   this.apiDate = this.datePipe.transform(this.date, 'yyyy-MM-dd');
  //   console.log(this.apiDate)
  //    }
}
