import { Component, OnInit, Input } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { NgForm, FormControl, FormGroup, Validators } from '@angular/forms';

import { CatalogService } from '../../../../../../../service/catalog.service';
import { EditComponent } from '../../edit.component';
import { Title } from '@angular/platform-browser';

import { faTrash } from '@fortawesome/free-solid-svg-icons';
import { MatSnackBar } from '@angular/material';
import { createSnackBarConfig } from 'src/app/models/snackbar';

@Component({
  selector: 'app-experience-details',
  templateUrl: './experience-details.component.html',
  styleUrls: ['./experience-details.component.css']
})

export class ExperienceDetailsComponent implements OnInit {
  @Input() product;
  forms: any={};
  disabled: boolean;
  disinput: boolean;
  faTrash = faTrash;

  itinForm = new FormGroup({
    time: new FormControl('', Validators.required),
    value: new FormControl('', Validators.required)
  })

  expInclude: any=[
    {typeOriginal: 'Food', type: '', value: '', enabled: false},
    {typeOriginal: 'Drink',type: '', value: '', enabled: false},
    {typeOriginal: 'Ticket', type: '', value: '', enabled: false},
    {typeOriginal: 'Transportation', type: '', value: '',enabled: false},
  ];

  constructor(
    private pageTitle: Title,
    private route: ActivatedRoute,
    private productService: CatalogService,
    private router: Router,
    private snackBar:  MatSnackBar
  ) { }

  ngOnInit() {
    this.pageTitle.setTitle('Manage Experience')
    this.getProduct();
  }

  getProduct():void{
    const id = +this.route.snapshot.paramMap.get('id');
    this.productService.editProductById(id)
    .subscribe(product => {
      this.product = product
      console.log(this.product)
      var exp_include = this.product.exp_include;

      if(exp_include != null) {
        for(var z=0; z < exp_include.length; z++)
        {
          if(exp_include[z].type != "") this.expInclude[z].enabled = true;
          else this.expInclude[z].enabled = false;

          this.expInclude[z].type=exp_include[z].type
          this.expInclude[z].value=exp_include[z].value == null ? '' : exp_include[z].value;
        }
      }

      //if(this.product.exp_exclude.length == 0) this.tambahh()
      if(this.product.exp_itenenary == null) this.product.exp_itenenary=[]

      /*
      if(this.product.exp_itenenary == null) this.product.exp_itenenary=[]
      if(this.product.exp_itenenary.length == 0) this.tambah()
      */
    });
  }

  saveFood(saveForm: NgForm):void{
    console.log(saveForm.value);
  }

  itin(event){
    console.log(event);
  }

  tambah(): void{
    let a={
      time: '',
      value:'',
    }
    if(this.product.exp_itenenary == null) this.product.exp_itenenary=[];

    this.product.exp_itenenary.push(a);
  }

  tambahh(): void {
    this.product.exp_exclude.push('');
  }

  nextStep(): void{
    EditComponent.changeStep(4);
    this.router.navigate(['account/customer/manage-experience/my-experience/edit/id/'+this.product.id], {queryParams: {step: 5}} )
  }

  simpan(){
    this.product.exp_include = this.expInclude
    
    this.productService.updateProduct(this.product, this.product.id)
    .subscribe((response) => {
      if(response.hasOwnProperty('message_dialog')){
  
      }else{
        this.snackBar.open("Experience Details has been updated successfully!",
            null,
            createSnackBarConfig('toast_success')
          );
        this.nextStep()
      }
    });
    
  }
  
  toggleProvide(index){
    if(this.expInclude[index].enabled == true) {
      this.expInclude[index].type = this.expInclude[index].typeOriginal;
    }
    else this.expInclude[index].type = "";
  }
}
