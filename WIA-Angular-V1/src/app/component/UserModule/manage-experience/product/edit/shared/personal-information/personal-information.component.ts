import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';

import { ActivatedRoute, Router } from '@angular/router';
import { UserService } from '../../../../../../../service/user.service';
import { CatalogService } from '../../../../../../../service/catalog.service'
import { User } from '../../../../../../../models/user';
import { Product } from '../../../../../../../models/product';
import { Title } from '@angular/platform-browser'
import { MatSnackBar } from '@angular/material';
import { createSnackBarConfig } from 'src/app/models/snackbar';

@Component({
  selector: 'app-personal-information',
  templateUrl: './personal-information.component.html',
  styleUrls: ['./personal-information.component.css']
})
export class PersonalInformationComponent implements OnInit {
  user: User;
  product: Product;
  forms: any={};

  constructor(
    private pageTitle: Title,
    private route: ActivatedRoute,
    private userService: UserService,
    private catalogService: CatalogService,
    private router: Router,
    public snackBar: MatSnackBar
  ) {
    // if(localStorage.getItem("customer_bio") == null){
    //   this.forms.customer_bios=''
    // }else{
    //   this.forms=JSON.parse(localStorage.getItem("customer_bio"));
    // }
   }

  ngOnInit() {
    this.pageTitle.setTitle('Manage Experience')
    this.getProfile();
  }

  getProfile():void{
    const id = +this.route.snapshot.paramMap.get('id');
    this.catalogService.getProductById(id)
    .subscribe(product => {
      this.product = product
    });

    this.userService.getUserProfile()
    .subscribe(user => {
      console.log(user)
      this.user = user
    });
  }

  saveCustomerBio(saveForm: NgForm):void{
    console.log(saveForm.value);
  }

  saveCustomerJob(saveForm: NgForm):void{
    console.log(saveForm.value);
  }

  nextStep(): void{
    this.router.navigate(['account/customer/manage-experience/my-experience/edit/id/'+this.product.id], {queryParams: {step: 2}} )
  }

  simpan():void{
    this.userService.updateUserProfile(this.user)
    .subscribe((response)=>{
      if(response.hasOwnProperty('message_dialog')){
  
      }else{
      this.snackBar.open("Personal information has been saved successfully",
        null,
        createSnackBarConfig('toast_success')
      );
      this.nextStep()
      }
    }
      );
  }
} 