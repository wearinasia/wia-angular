import { Component, OnInit, Input } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { CatalogService } from '../../../../../../../service/catalog.service';
import { Title } from '@angular/platform-browser';

import {
  MatSnackBar,
  MatSnackBarConfig,
  MatSnackBarHorizontalPosition,
  MatSnackBarVerticalPosition,
} from '@angular/material';
import { createSnackBarConfig } from 'src/app/models/snackbar';

@Component({
  selector: 'app-basic-information',
  templateUrl: './basic-information.component.html',
  styleUrls: ['./basic-information.component.css']
})
export class BasicInformationComponent implements OnInit {
  @Input() product;
  

  editForm = new FormGroup({
    name: new FormControl('', [
      Validators.required,
    ]),
    short_description: new FormControl('', [
      Validators.required,
    ]),
    description: new FormControl('', [
      Validators.required,
    ]),
  });

  constructor(
    private pageTitle: Title,
    private productService: CatalogService,
    private router: Router,
    public snackBar: MatSnackBar
  ) {}

  ngOnInit() { 
    this.pageTitle.setTitle('Manage Experience')
    console.log(this.product);
    this.editForm.patchValue(this.product);
  }

  onSubmit(){
    
    this.productService.updateProduct(this.editForm.value, this.product.id)
    .subscribe(response=>
      {
        if(response.hasOwnProperty('message_dialog')){
        }else{
          this.snackBar.open("Basic information of the product has been updated",
            null,
            createSnackBarConfig('toast_success')
          );
          this.router.navigate(['account/customer/manage-experience/my-experience/edit/id/'+this.product.id], {queryParams: {step: 3}} )
        }
      }
    );
  }
}