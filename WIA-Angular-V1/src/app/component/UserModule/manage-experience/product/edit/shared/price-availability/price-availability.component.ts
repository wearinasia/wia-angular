import { Component, OnInit, Input } from '@angular/core';
import { FormGroup,FormControl, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { CatalogService } from '../../../../../../../service/catalog.service';
import { Title } from '@angular/platform-browser';
import { MatSnackBar } from '@angular/material';
import { createSnackBarConfig } from 'src/app/models/snackbar';

@Component({
  selector: 'app-price-availability',
  templateUrl: './price-availability.component.html',
  styleUrls: ['./price-availability.component.css']
})
export class PriceAvailabilityComponent implements OnInit {
  @Input() product;
  
  editFormm = new FormGroup({
    price: new FormControl('', Validators.required),
    tetapanqty: new FormControl('', Validators.required),
    exp_location: new FormControl('', Validators.required),
  });

  locations: any[] = [
     'Jakarta',
     'Tangerang',
     'Bali',
     'Bandung',
     'Bogor',
     'Wonosobo'
  ]

  constructor(
    private pageTitle: Title,
    private productService: CatalogService,
    private router: Router,
    private snackBar: MatSnackBar,
  ) { }

  ngOnInit( ) {
    this.product.price=parseInt(this.product.price,10)
    this.pageTitle.setTitle('Manage Experience')
    this.locations=this.locations.sort();
    this.editFormm.patchValue(this.product);
  }

  onSubmit(){
    this.productService.updateProduct(this.editFormm.value, this.product.id)
    .subscribe(response=>{
        if(response.hasOwnProperty('message_dialog')){
  
        }else{
          this.snackBar.open("Price and availability has been updated successfully!",
            null,
            createSnackBarConfig('toast_success')
          );
          this.router.navigate(['account/customer/manage-experience/my-experience/edit/id/'+this.product.id], {queryParams: {step: 4}} )
        }
      }
    );
  }
}