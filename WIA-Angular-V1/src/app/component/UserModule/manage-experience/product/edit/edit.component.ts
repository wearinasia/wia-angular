import { Component, OnInit, Input } from '@angular/core';
import { CatalogService } from '../../../../../service/catalog.service';
import { Product } from '../../../../../models/product'
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-edit',
  templateUrl: './edit.component.html',
  styleUrls: ['./edit.component.css']
})
export class EditComponent implements OnInit {
  product: Product;
  forms: any={}

  static step: number;

  ngOnInit() {
    this.getProduct();
  }

  getProduct():void{
    const id = +this.route.snapshot.paramMap.get('id');
    this.productService.editProductById(id)
    .subscribe(product => this.product = product);
  }

  constructor(
    private route: ActivatedRoute,
    private productService: CatalogService,
  ) 
  {
    EditComponent.step=0
    this.getProduct();
  }

  static changeStep(s){
    EditComponent.step=s;
  }

  getStep(){
    let step = this.route.snapshot.queryParamMap.get("step")
  return step;
  }

}
