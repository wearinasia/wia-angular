import { Injectable, Input } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router, ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { Product } from 'src/app/models/product';
import { AuthService } from 'src/app/service/auth.service';
import { MatSnackBar } from '@angular/material';
import { createSnackBarConfig } from 'src/app/models/snackbar';

@Injectable()
export class FormGuard implements CanActivate {
    @Input() product: Product;
    constructor(
        private router: Router,
        private snackBar:  MatSnackBar,
        private authService: AuthService
        ) {}
  
    canActivate(
        route: ActivatedRouteSnapshot,
        state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
            var params = route.queryParams
            var customer_id = params['customerID'];
            var token = params['token'];
            if(customer_id != undefined && token != undefined){
                var user={
                    'customer_id': customer_id,
                    'token': token,
                }
                this.authService.setCustomerSession(user)
            }
            
            if(!this.authService.isAuthenticated()){
                this.snackBar.open("Please login to access this page",
                    null,
                    createSnackBarConfig('toast_warning')
                );
                this.router.navigate(['account/login'])
            }
            
            if (this.router.url === '/product/edit/:id', {queryParams: {step: 1}}) {
                console.log(this.router.url === '/product/edit/:id', {queryParams: {step: 1}})
                return true;
            } else {
                alert("Please complete step 1")
                this.router.navigate(['product/edit/:id'], {queryParams: {step: 1}} )
                console.log(this.router.navigate(['product/edit/:id'], {queryParams: {step: 1}} ))
            return false
            }
        }
    }