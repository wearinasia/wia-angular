import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { Product } from '../../../../models/product';
import { CatalogService } from '../../../../service/catalog.service'
import { Router, ActivatedRoute } from '@angular/router';
import { DatePipe } from '@angular/common';
import { Title } from '@angular/platform-browser';

@Component({
  selector: 'app-product',
  templateUrl: './product.component.html',
  styleUrls: ['./product.component.css'],
  encapsulation: ViewEncapsulation.None,
})

export class ProductComponent implements OnInit {
  products: Product[];
  prod: Product;
  product_id: number;
  date: any;
  apiDate: any;
  minDate = new Date();
  private customerSession = JSON.parse(localStorage.getItem("customerSession"));
  token: string = this.customerSession.token;
  customer_id : string = this.customerSession.customer_id;

  constructor(private pageTitle: Title, private productService: CatalogService, private router: Router, private route: ActivatedRoute, private datePipe : DatePipe) { }

  ngOnInit() {
    this.pageTitle.setTitle('My Experience')
    // this.customer_id = JSON.parse(localStorage.getItem('customer_id'));
    this.getProduct();
  }

  getProduct():void{
    this.productService.getProductsByCustomerId(this.customer_id)
    .subscribe(products => 
      this.products = products
      );
  }

  addProduct(): void {
    this.productService.addProduct(this.customer_id)
      .subscribe(product => {
        console.log(product)
        console.log(this.token)
        this.product_id = product.id
        this.router.navigateByUrl('account/customer/manage-experience/my-experience/edit/id/'+ this.product_id + '?step=1')
      });
  }
}
