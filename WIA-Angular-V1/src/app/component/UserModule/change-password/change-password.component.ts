import { Component, OnInit } from '@angular/core';

import { Router } from '@angular/router';
import { AuthService } from '../../../service/auth.service';
import { FormControl, Validators, FormGroup } from '@angular/forms';
import { Auth } from 'src/app/models/auth';
import { Title } from '@angular/platform-browser';

@Component({
  selector: 'app-change-password',
  templateUrl: './change-password.component.html',
  styleUrls: ['./change-password.component.css']
})
export class ChangePasswordComponent implements OnInit {
  changePassForm = new FormGroup({
  email: new FormControl('', Validators.required),
  password: new FormControl('', Validators.required),
  new_password: new FormControl('', Validators.required),
  });
  
  auth: Auth;
  
  constructor(
    private pageTitle: Title,
    private authService: AuthService,
    private router: Router
  ) { }

  ngOnInit() {
    this.pageTitle.setTitle('Change Password')
  }

  onSubmit(): void{
    this.authService.changePassword(this.changePassForm.value)
    .subscribe(auth => {
      //console.log("Password Changed!");
      alert(auth.message_code);
      //this.router.navigateByUrl('account/customer/profile');
    })
  }

}
