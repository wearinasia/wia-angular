import { Component, OnInit } from '@angular/core';
import { Title } from '@angular/platform-browser';
import { AuthService } from 'src/app/service/auth.service';
import { Auth } from 'src/app/models/auth';
import { Router } from '@angular/router';
import { MatSnackBar } from '@angular/material';
import { createSnackBarConfig } from 'src/app/models/snackbar';

@Component({
  selector: 'app-account',
  templateUrl: './account.component.html',
  styleUrls: ['./account.component.css']
})
export class AccountComponent implements OnInit {
  isLoggedIn: Boolean

  constructor(private pageTitle: Title, private authService: AuthService,
    private router: Router,
    private snackBar: MatSnackBar) { }

  ngOnInit() {
    this.pageTitle.setTitle('My Account');
    this.isLoggedIn =this.authService.isAuthenticated()
  }

  logoutAccount(){
    if(this.authService.logout()) {
      this.snackBar.open("You have been logged out!",
        null,
        createSnackBarConfig('toast_success')
      );
      this.router.navigate(['/']);
    }
  }

}
