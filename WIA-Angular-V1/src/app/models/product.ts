export class Product {
  id: number;
  url_key: string;
  name: string;
  price: string;
  description: string;
  short_description: string;
  customer_id: string;
  exp_available_date?: (string)[] | null;
  exp_duration: string;
  exp_include?: (ExpIncludeEntity)[] | null;
  exp_exclude?: (string)[] | null;
  exp_location: string;
  exp_itenenary?: (ExpItenenaryEntity)[] | null;
  tetapanqty: string;
  media_gallery: MediaGallery;
  category: string;
  host: Host;
}
export class ExpIncludeEntity {
  type?: string | null;
  value: string;
}
export class ExpItenenaryEntity {
  time: string;
  value: string;
}
export class MediaGallery {
  images?: (ImagesEntity)[] | null;
  values?: (null)[] | null;
}
export class ImagesEntity {
  value_id: string;
  file: string;
  product_id: string;
  label: string;
  position: string;
  disabled: string;
  label_default: string;
  position_default: string;
  disabled_default: string;
}
export class Host{
  name: string;
  bio: string;
  photo: string;
}