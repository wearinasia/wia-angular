export class Customer {
    email: string;
    password: string;
    firstname: string;
    lastname: string;
    customer_id: string;
    token : string;
}