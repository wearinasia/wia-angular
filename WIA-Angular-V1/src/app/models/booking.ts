export class Booking {
    availability: string;
    id: string;
    product_id: string;
    name: string;
    status: string;
    customer_id: string;
    customer_name: string;
    customer_email: string;
    phone: string;
    booking_date?: string | null;
    qty: string;
    sub_total: string;
    shipping_cost: string;
    voucher: string;
    base_grand_total: string;
    grand_total: string;
    coupon_code: string;
    guest_info: GuestInfo;
  }
  export class GuestInfo {
    note: string;
  }
  