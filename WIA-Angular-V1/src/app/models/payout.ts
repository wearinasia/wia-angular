export class Payout {
    total_payout: number;
    transactions?: (TransactionsEntity)[] | null;
  }
  export class TransactionsEntity {
    id: string;
    product_id: string;
    name: string;
    state: string;
    status: string;
    customer_id: string;
    customer_name: string;
    customer_email: string;
    phone: string;
    booking_date: string;
    guest_info: GuestInfo | string;
    price: string;
    qty: string;
    sub_total: string;
    shipping_cost: string;
    voucher: string;
    base_grand_total: string;
    grand_total: string;
    base_image: string;
    thumbnail: string;
    small_image: string;
  }
  export class GuestInfo {
    name?: string | null;
    email?: string | null;
    phone?: number | string;
    note: string;
    customer_name?: string | null;
    customer_email?: string | null;
    customer_phone?: string | null;
  }
  