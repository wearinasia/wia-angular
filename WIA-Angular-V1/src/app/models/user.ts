export class User {
  id: string;
  firstname: string;
  lastname: string;
  email: string;
  photo: string;
  customer_bio: string;
  customer_job: string;
  address: Address;
  is_active: string;
  rating: string;
  verified: string;
  fotoktp: string;
}
export class Address {
  company: string;
  city: string;
  region: string;
  postcode?: null;
  country_id: string;
  telephone: string;
  region_id: number;
  street?: (string)[] | null;
}
