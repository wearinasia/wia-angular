export enum orderStatus{
    processing=0,
    processing_checkin=1,
    processing_checkout=2,
    complete_payout_request=3,
    complete=4
}
  
export enum labelStatusAsHost{
    processing="Ready to Start",
    processing_checkin = "In Process",
    processing_checkout = "Experience Completed",
    complete_payout_request = "Experience Completed - Payment has been requested",
    complete = "Experience Completed - Rating & Payment has been requested "
}

export enum labelPayout{
    processing_checkout = "Payout available",
    complete_payout_request = "Waiting for payment",
    complete = "Paid"
}

export enum labelStatusAsCustomer{
    pending="Waiting for payment",
    processing="Ready to Start",
    processing_checkin = "In Process",
    processing_checkout = "Experience Completed",
    complete_payout_request = "Experience Completed",
    complete="Experience Completed"
}
  