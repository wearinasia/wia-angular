export class Review {
    id: string;
    product_id: string;
    status_id: string;
    title: string;
    detail: string;
    created: string;
    customer: Customer;
    value: string;
    percent: string;
  }
  export class Customer {
    customer_id: string;
    name: string;
    photo?: null;
    bio: string;
  }
  