export class Category {
  category_id: string;
  category_name: string;
  subcategory?: null;
  products?: (ProductsEntity)[] | null;
}
export class ProductsEntity {
  id: string;
  url_key: string;
  name: string;
  price: string;
  description?: null;
  short_description: string;
  customer_id: string;
  category?: (string)[] | null;
  exp_host_partner?: null;
  exp_available_date?: (string)[] | null;
  exp_duration?: string | null;
  exp_include?: (ExpIncludeEntity)[] | null;
  exp_exclude?: (string)[] | null;
  exp_location: string;
  exp_itenenary?: (ExpItenenaryEntity)[] | null;
  tetapanqty: string;
  base_image?: null;
  thumbnail: string;
  small_image: string;
}
export class ExpIncludeEntity {
  value: string;
  type?: string | null;
}
export class ExpItenenaryEntity {
  time: string;
  value: string;
}
