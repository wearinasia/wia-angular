import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ProductComponent } from './component/UserModule/manage-experience/product/product.component';
import { EditComponent } from './component/UserModule/manage-experience/product/edit/edit.component';

import { ListComponent } from './component/CatalogModule/list/list.component';
import { ViewComponent } from './component/CatalogModule/view/view.component';
import { FormGuard } from './component/UserModule/manage-experience/product/edit/shared/form.guard';

import { LoginComponent } from './component/AuthModule/login/login.component';
import { ForgotPasswordComponent } from './component/AuthModule/forgot-password/forgot-password.component'
import { RegisterComponent } from './component/AuthModule/register/register.component';
import { AuthGuard } from './component/AuthModule/auth.guard';
import { AddComponent } from './component/BookingModule/add/add.component';

import { MyOrdersComponent } from './component/UserModule/manage-experience/order/my-orders/my-orders.component';
import { MyOrdersViewComponent } from './component/UserModule/manage-experience/order/my-orders-view/my-orders-view.component';
import { CalendarrComponent } from './shared/calendar/calendar.component';
import { MyBookingComponent } from './component/UserModule/booking/my-booking/my-booking.component';
import { MyBookingViewComponent } from './component/UserModule/booking/my-booking-view/my-booking-view.component';
import { MyProfileComponent } from './component/UserModule/profile/my-profile/my-profile.component';
import { HomepageComponent } from './component/CatalogModule/homepage/homepage.component';

import { MyProfileEditComponent } from './component/UserModule/profile/my-profile-edit/my-profile-edit.component';
import { ChangePasswordComponent } from './component/UserModule/change-password/change-password.component';
import { PayoutListComponent } from './component/UserModule/manage-experience/payout/payout-list/payout-list.component';
import { BankAccountListComponent } from './component/UserModule/manage-experience/bank-account/bank-account-list/bank-account-list.component';
import { BankAccountViewComponent } from './component/UserModule/manage-experience/bank-account/bank-account-view/bank-account-view.component';
import { PayoutViewComponent } from './component/UserModule/manage-experience/payout/payout-view/payout-view.component';
import { AccountComponent } from './component/UserModule/account/account.component';
import { ManageExperienceComponent } from './component/UserModule/manage-experience/manage-experience.component';
import { SearchComponent } from './component/CatalogModule/search/search.component';
import { PhotoProfileEditComponent } from './component/UserModule/profile/photo-profile-edit/photo-profile-edit.component';
import { PromoListComponent } from './component/CatalogModule/promo/promo-list/promo-list.component';
import { PromoViewComponent } from './component/CatalogModule/promo/promo-view/promo-view.component';
import { LandingpageComponent } from './component/UserModule/manage-experience/onboarding/landingpage/landingpage.component';
import { OnboardingStep1Component } from './component/UserModule/manage-experience/onboarding/onboarding-step1/onboarding-step1.component';
import { OnboardingStep2Component } from './component/UserModule/manage-experience/onboarding/onboarding-step2/onboarding-step2.component';
import { OnboardingStep3Component } from './component/UserModule/manage-experience/onboarding/onboarding-step3/onboarding-step3.component';


const routes: Routes = [
  
  { path: '', redirectTo: '/home', pathMatch: 'full' },
  { path: 'home', component: HomepageComponent},
  { path: 'account/customer/manage-experience/my-experience', component: ProductComponent, canActivate: [AuthGuard]},
  { path: 'account/customer/manage-experience/my-experience/edit/id/:id', component: EditComponent, canActivate: [FormGuard]},

  { path: 'account/customer/manage-experience/orders/list', component: MyOrdersComponent, canActivate: [AuthGuard]},
  { path: 'account/customer/manage-experience/orders/view/id/:id', component: MyOrdersViewComponent, canActivate: [AuthGuard]},

  { path: 'account/customer/manage-experience/payout-request', component: PayoutListComponent ,canActivate: [AuthGuard]},
  { path: 'account/customer/manage-experience/payout-request/view/id/:id', component: PayoutViewComponent, canActivate: [AuthGuard]},
  
  
  { path: 'account/customer/manage-experience/bank-account/list', component: BankAccountListComponent, canActivate: [AuthGuard]},
  { path: 'account/customer/manage-experience/bank-account/view', component: BankAccountViewComponent, canActivate: [AuthGuard]},
  { path: 'account/customer/manage-experience/bank-account/view/id/:id', component: BankAccountViewComponent, canActivate: [AuthGuard]},

  { path: 'account/customer', component: AccountComponent},
  { path: 'account/customer/manage-experience', component: ManageExperienceComponent, canActivate: [AuthGuard]},
  { path: 'account/customer/profile', component: MyProfileComponent, canActivate: [AuthGuard]},
  { path: 'account/customer/profile/edit', component: MyProfileEditComponent , canActivate: [AuthGuard]},
  { path: 'account/customer/photo/edit', component: PhotoProfileEditComponent, canActivate: [AuthGuard]},
  { path: 'account/customer/booking/list', component: MyBookingComponent, canActivate: [AuthGuard]},
  { path: 'account/customer/booking/view/id/:id', component: MyBookingViewComponent, canActivate: [AuthGuard]},
  { path: 'account/customer/change-password', component: ChangePasswordComponent, canActivate: [AuthGuard]},
  
  { path: 'catalog/search', component: SearchComponent},
  { path: 'catalog/category', component: ListComponent},
  { path: 'catalog/category/id/:id', component: ListComponent},
  { path: 'catalog/product/id/:id', component: ViewComponent},

  { path: 'catalog/promo', component: PromoListComponent},
  { path: 'catalog/promo/id/:id', component: PromoViewComponent},

  { path: 'account/register', component: RegisterComponent},
  { path: 'account/login', component: LoginComponent},
  { path: 'forgot-password', component: ForgotPasswordComponent},

  { path: 'booking-details', component: AddComponent ,canActivate: [AuthGuard]},

  { path: 'calendar', component: CalendarrComponent},

  {path: 'onboarding', component: LandingpageComponent},
  {path: 'onboarding/step-1', component: OnboardingStep1Component},
  {path: 'onboarding/step-2', component: OnboardingStep2Component},
  {path: 'onboarding/step-3', component: OnboardingStep3Component},


];

@NgModule({
  imports: [ RouterModule.forRoot(routes, {onSameUrlNavigation: 'reload'}) ],
  exports: [ RouterModule ]
})
export class AppRoutingModule { }
