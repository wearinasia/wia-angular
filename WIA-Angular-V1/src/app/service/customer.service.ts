import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { Customer } from '../models/customer';

@Injectable({ providedIn: 'root' })
export class UserService {
  private loginUrl = 'https://wia.id/v1/Auth/Login';
  private registerUrl = 'https://wia.id/v1/Auth/Register';
    constructor(private http: HttpClient) { }

    getAll() {
        return this.http.get<Customer[]>(this.loginUrl);
    }

    getById(id: number) {
        return this.http.get(this.loginUrl + id);
    }

    register(user: Customer) {
        return this.http.post(this.registerUrl, user);
    }

    // update(user: Customer) {
    //     return this.http.put(`/users/` + user.id, user);
    // }

    // delete(id: number) {
    //     return this.http.delete(`/users/` + id);
    // }
}