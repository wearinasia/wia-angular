import { Injectable } from '@angular/core';
import { Review } from '../models/review';
import { Observable } from 'rxjs';
import { HttpHeaders, HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class ReviewService {
  private Url = 'https://wia.id/v1/Review';
  private token: string;

  private httpOptions = {
    headers: new HttpHeaders({
      'source': 'source-app',
      'Authorization': `${this.token}`
    })
  }

  constructor(private http: HttpClient) { }

  getReview(id: number): Observable<Review>{
  return this.http.get<Review>(this.Url + '?product=' + id, this.httpOptions)
  }

  addReview(form, product_id: number): Observable<any>{
    return this.http.post<any>(this.Url + '?product=' + product_id, form, this.httpOptions)
    }
}