import { Injectable, OnInit } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { Customer } from '../models/customer'
import { Auth } from '../models/auth';

@Injectable({ providedIn: 'root' })
export class AuthService implements OnInit{

    private loginUrl = 'https://wia.id/v1/Auth/Login';
    private registerUrl = 'https://wia.id/v1/Auth/Register';
    private forgotUrl = 'https://wia.id/v1/Auth/ForgotPassword';
    private changePassUrl = 'https://wia.id/v1/Auth/ChangePassword'
    customer: Customer;
    private token: string;
    isLoggedIn: boolean;
    customer_id: string;
    auth: Auth;

    private authOptions = {
      headers: new HttpHeaders({
        'source': 'source-app',
        'Authorization': `${this.token}`
      })
    }

    ngOnInit(): void{}

    constructor(private http: HttpClient){}

      login(form): Observable<Auth>{
          return this.http.post<Auth>(this.loginUrl,form)
      }

      
      register(form): Observable<Auth>{
        return this.http.post<Auth>(this.registerUrl,form)
      }

      private handleError<T> (operation = 'operation', result?: T) {
        return (error: any): Observable<T> => {
    
          console.error(error);
          console.log(`${operation} failed: ${error.message}`);
          return of(result as T);
        };
      }

      changePassword(form): Observable<Auth>{
        return this.http.post<Auth>(this.changePassUrl, form, this.authOptions)
      }

      forgot(idtoken: string, password: string): Observable<Customer>{
          console.log('Forgot Password');
          this.http.post(this.forgotUrl,{
              idToken: idtoken,
              password: password,
              returnSecureToken: "true"
          })
          .subscribe(
              response => console.log(response),
              error => console.log(error)
          )

          return this.http.post<Customer>(this.forgotUrl,{
              idToken: idtoken,
              password: password,
              returnSecureToken: "true"
          })
      }

      logout(){
          localStorage.removeItem('customerSession');
          return true;
      }

      getRegisterSession(authResult){
          const session = JSON.stringify(authResult);
          this.getRegisterSession(session);
      }

      getLoginSession(authResult){
          const session = JSON.stringify(authResult);
          this.getLoginData(session);
          console.log(session);
      }

      getLoginData(getSession){
          const session = JSON.parse(getSession);
          console.log(JSON.stringify(session.token));
          this.token =  session.token;
          this.customer_id = session.customer_id;
          localStorage.setItem('customerSession', 
          JSON.stringify({token : JSON.stringify(this.token), 
            customer_id : JSON.stringify(this.customer_id)
        }));
      }

      setLoggedIn(loggedIn){
        this.auth = JSON.parse(localStorage.getItem('customerSession'));
        this.isLoggedIn = loggedIn;
      }

      getLoggedIn(): boolean{
        this.auth = JSON.parse(localStorage.getItem('customerSession'));
        if(this.auth != null){
          return true;
        }
        else{
          return false;
        }
      }

      setCustomerSession(authResult){
        localStorage.setItem('customerSession', JSON.stringify(authResult));
      }

      getCustomerToken(){
        this.auth = JSON.parse(localStorage.getItem('customerSession'));
        if(this.auth == null) return null;
        else return this.auth.token;
      }

      isAuthenticated(): Boolean{
        this.auth = JSON.parse(localStorage.getItem('customerSession'));
        if(this.auth == null) return false;
        else return true;
      }
}