import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Booking } from '../models/booking';
import { Payout } from '../models/payout';
import { Observable } from 'rxjs';
import { AuthService } from './auth.service';

@Injectable({
  providedIn: 'root'
})
export class BookingService {
  private Url = 'https://wia.id/v1/booking';
  private paymentUrl = 'https://api.sandbox.midtrans.com/v2/charge';
  private token: string;

  private bookingOptions = {
    headers: new HttpHeaders({
      'source': 'source-app',
      'Authorization': `${this.token}`
    })
  }

  constructor(private http: HttpClient, private authService: AuthService) { }

  setBookingOptions(){
    this.bookingOptions.headers = this.bookingOptions.headers.set('Authorization', `${this.authService.getCustomerToken()}`)
  }

  addBooking(form): Observable<Booking>{
    this.setBookingOptions();
    return this.http.post<Booking>(this.Url, form, this.bookingOptions)
  }

  getOrdersList(): Observable<Booking[]>{
    this.setBookingOptions();
    return this.http.get<Booking[]>(this.Url + '/MyOrders', this.bookingOptions)
  }

  getBookingDetail(booking_id: number): Observable<Booking>{
    this.setBookingOptions();
    return this.http.get<Booking>(this.Url + '?booking=' + booking_id, this.bookingOptions
    )
  }

  getOrderDetail(booking_id: number): Observable<Booking>{
    this.setBookingOptions();
    return this.http.get<Booking>(this.Url + '/Details?order=' + booking_id, this.bookingOptions
    )
  }

  getPayoutDetail(booking_id: number): Observable<Booking>{
    this.setBookingOptions();
    return this.http.get<Booking>(this.Url + '/Details?payout=' + booking_id, this.bookingOptions
    )
  }

  getHistoryList(): Observable<Booking[]>{
    this.setBookingOptions();
    return this.http.get<Booking[]>(this.Url + '/MyBooking', this.bookingOptions
    )
  }

  getPayout(): Observable<Payout>{
    this.setBookingOptions();
    return this.http.get<Payout>(this.Url + '/payout', this.bookingOptions)
  }

  checkAvailability(product_id: number, booking_date: string): Observable<Booking>{
    const bookingOptions = {
      headers: new HttpHeaders({
        'source': 'source-app'
      })
    }
    return this.http.get<Booking>(this.Url + '/check?product=' + product_id + '&bookdate=' + booking_date, bookingOptions)
  }

  checkVoucher(coupon_code: string): Observable<any>{
    this.setBookingOptions();
    return this.http.get<any>(this.Url + '/CheckVoucher?coupon=' + coupon_code, this.bookingOptions)
  }


  changeBookingStatus(orderID: string, nextStatus: string): Observable<any>{
    this.setBookingOptions();
    return this.http.put<any>(this.Url + '/update',
    {
      "booking": orderID,
      "status": nextStatus
    }, this.bookingOptions)
  }
}
