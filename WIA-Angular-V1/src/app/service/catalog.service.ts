import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { Product } from '../models/product'
import { Category } from '../models/category'
import { AuthService } from './auth.service';

@Injectable({ providedIn: 'root'})
export class CatalogService {
  private Url = 'https://wia.id/v1/catalog/';
  private token: string;

  private httpOptions = {
    headers: new HttpHeaders({
      'source': 'source-app',
      'Authorization': `${this.token}`
    })
  }

  constructor(private http: HttpClient, private authService: AuthService) { }

  setHttpOptions(){
    this.httpOptions.headers = this.httpOptions.headers.set('Authorization', `${this.authService.getCustomerToken()}`)
  }

  uploadPhoto(image): Observable<any>{
    this.setHttpOptions();
    return this.http.post(this.Url+'ProductUpload',image, this.httpOptions)
  }

  updateProduct(form, product_id: Number): Observable<any> {
    this.setHttpOptions();
    return this.http.put(this.Url + '?product=' + product_id, form, this.httpOptions)
  }

  getProductsByCustomerId(customer_id: string): Observable<Product[]>{
    this.setHttpOptions();
    return this.http.get<Product[]>(this.Url + 'customer?customer_id=' + customer_id, this.httpOptions)
  }

  getProducts(): Observable<Product[]>{
    return this.http.get<Product[]>(this.Url)
  }

  addProduct(customer_id: string): Observable<Product> {
    this.setHttpOptions();
    let body = ''
    return this.http.post<Product>(this.Url + '?customer_id=' + customer_id, body, this.httpOptions)
  }

  getProductById(id: number): Observable<Product>{
    const url = `${this.Url}?product=${id}`;
    return this.http.get<Product>(url)
}

  editProductById(id: number): Observable<Product>{
    this.setHttpOptions();
    const url = `${this.Url}?product=${id}`;
    return this.http.get<Product>(url, this.httpOptions)
  }

  getProductsByCategory(category_id: number): Observable<Category>{
    this.setHttpOptions();
    return this.http.get<Category>(this.Url + 'category?id=' + category_id, this.httpOptions)
  }

  getProductsByCategoryId(category_id: number): Observable<Category>{
    return this.http.get<Category>(this.Url + 'category?id=' + category_id)
  }

  getPromoCategory(category_id: number): Observable<Category>{
    return this.http.get<Category>(this.Url + 'category?id=' + category_id)
  }
}
