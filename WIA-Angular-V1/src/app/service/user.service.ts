import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { User } from '../models/user';
import { Observable } from 'rxjs';
import { AuthService } from './auth.service';

@Injectable({
    providedIn: 'root'
  })
  export class UserService {
    private Url = 'https://wia.id/v1/user_profile';
    private token: string;

    private userOptions = {
        headers: new HttpHeaders({
            'Source': 'source-app',
            'Authorization': `${this.token}`
        })
    }
  
    constructor(private http: HttpClient, private authService: AuthService) { }

    setUserOptions(){
        this.userOptions.headers = this.userOptions.headers.set('Authorization', `${this.authService.getCustomerToken()}`)
    }

    getUserProfile(): Observable<User>{
        this.setUserOptions();
        return this.http.get<User>(this.Url, this.userOptions)
    }

    getBank(): Observable<any>{
        this.setUserOptions();
        return this.http.get<any>(this.Url + '/bank', this.userOptions)
    }

    addBank(bankForm): Observable<User>{
        this.setUserOptions();
        return this.http.post<User>(this.Url + '/bank', bankForm, this.userOptions)
    }

    editBank(bankForm, acc_id): Observable<User>{
        this.setUserOptions();
        return this.http.put<User>(this.Url + '/bank?id=' + acc_id, bankForm, this.userOptions)
    }

    updateUserProfile(editProfile): Observable<any>{
        this.setUserOptions();
        return this.http.put(this.Url + '/editcustomer', editProfile, this.userOptions)
    }

    changeProfilePhoto(image): Observable<any>{
        this.setUserOptions();
        return this.http.post(this.Url + '/ProfileUpload', image, this.userOptions)
    }
  }