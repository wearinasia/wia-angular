//Angular Module
import { BrowserModule } from '@angular/platform-browser';
import { NgModule, Pipe } from '@angular/core';
import { FormsModule, ReactiveFormsModule }   from '@angular/forms';
import { MatCardModule } from '@angular/material/card';
import { MatRadioModule } from '@angular/material/radio';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { AppRoutingModule } from './app-routing.module';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HttpClientModule } from '@angular/common/http';


//Shared Component
import { AppComponent } from './app.component';
import { HomeComponent } from './shared/header/home/header-home.component';
import { HeaderComponent } from './shared/header/myExp/header.component';
import { CardExperienceProductComponent } from './shared/card/card-experience-product/card-experience-product.component';

//Material Module
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatIconModule } from '@angular/material/icon';
import { MatInputModule } from '@angular/material/input';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatSelectModule } from '@angular/material/select';
import { MatNativeDateModule } from '@angular/material/core';
import { MatDialogModule } from '@angular/material/dialog';
import {MatAutocompleteModule} from '@angular/material/autocomplete';

//Auth Component
import { LoginComponent } from './component/AuthModule/login/login.component';
import { ForgotPasswordComponent } from './component/AuthModule/forgot-password/forgot-password.component';
import { RegisterComponent } from './component/AuthModule/register/register.component';

//User Component
import { ProductComponent } from './component/UserModule/manage-experience/product/product.component';
import { EditComponent } from './component/UserModule/manage-experience/product/edit/edit.component';
import { PhotoUploadComponent } from './component/UserModule/manage-experience/product/edit/shared/photo-upload/photo-upload.component';
import { PersonalInformationComponent } from './component/UserModule/manage-experience/product/edit/shared/personal-information/personal-information.component';
import { BasicInformationComponent } from './component/UserModule/manage-experience/product/edit/shared/basic-information/basic-information.component';
import { PriceAvailabilityComponent } from './component/UserModule/manage-experience/product/edit/shared/price-availability/price-availability.component';
import { ExperienceDetailsComponent } from './component/UserModule/manage-experience/product/edit/shared/experience-details/experience-details.component';
import { FormGuard } from './component/UserModule/manage-experience/product/edit/shared/form.guard';

import { MyOrdersComponent } from './component/UserModule/manage-experience/order/my-orders/my-orders.component';
import { MyOrdersViewComponent } from './component/UserModule/manage-experience/order/my-orders-view/my-orders-view.component';
import { MyBookingComponent } from './component/UserModule/booking/my-booking/my-booking.component';

//Catalog Component
import { ListComponent } from './component/CatalogModule/list/list.component';
import { ViewComponent} from './component/CatalogModule/view/view.component';
import { ListExperienceComponent } from './component/CatalogModule/list/list-experience/list-experience.component';

//Booking Component
import { AddComponent } from './component/BookingModule/add/add.component';
import { BookingDetailsComponent } from './component/BookingModule/add/shared/booking-details/booking-details.component';
import { BookingReviewComponent } from './component/BookingModule/add/shared/booking-review/booking-review.component';

//Global
import { AngularFontAwesomeModule } from 'angular-font-awesome';

//Testing
import { CalendarModule } from '@syncfusion/ej2-angular-calendars';

import { DatePipe } from '@angular/common';
import { CalendarrComponent } from './shared/calendar/calendar.component';
import { MyProfileComponent } from './component/UserModule/profile/my-profile/my-profile.component';
import { HomepageComponent } from './component/CatalogModule/homepage/homepage.component';
import { MyExpComponent } from './shared/my-exp/my-exp.component';
import { AnimateComponent } from './shared/animate/animate.component';
import { GlobalHeaderComponent } from './shared/header/global-header/global-header.component';
import { GlobalFooterComponent } from './shared/footer/global-footer/global-footer.component';
import { FeaturedLocationBlockComponent } from './shared/block/featured-location-block/featured-location-block.component';
import { FeaturedProductBlockComponent } from './shared/block/featured-product-block/featured-product-block.component';
import { FeaturedActivityBlockComponent } from './shared/block/featured-activity-block/featured-activity-block.component';
import { CardExperienceLocationComponent } from './shared/card/card-experience-location/card-experience-location.component';
import { CardExperienceActivityComponent } from './shared/card/card-experience-activity/card-experience-activity.component';
import { CardExperienceFeaturedProductComponent } from './shared/card/card-experience-featured-product/card-experience-featured-product.component';
import { CardBecomeHostComponent } from './shared/card/card-become-host/card-become-host.component';
import { CardExperienceProductDetailsComponent } from './shared/card/card-experience-product-details/card-experience-product-details.component';
import { BookingPaymentComponent } from './component/BookingModule/add/shared/booking-payment/booking-payment.component';
import { SidebarAccountComponent } from './shared/sidebar/sidebar-account/sidebar-account.component';
import { SidebarMyExperienceComponent } from './shared/sidebar/sidebar-my-experience/sidebar-my-experience.component';
import { PayoutListComponent } from './component/UserModule/manage-experience/payout/payout-list/payout-list.component';
import { PayoutViewComponent } from './component/UserModule/manage-experience/payout/payout-view/payout-view.component';
import { BankAccountListComponent } from './component/UserModule/manage-experience/bank-account/bank-account-list/bank-account-list.component';
import { BankAccountViewComponent } from './component/UserModule/manage-experience/bank-account/bank-account-view/bank-account-view.component';
import { MyProfileEditComponent } from './component/UserModule/profile/my-profile-edit/my-profile-edit.component';
import { ChangePasswordComponent } from './component/UserModule/change-password/change-password.component';
import { MyBookingViewComponent } from './component/UserModule/booking/my-booking-view/my-booking-view.component';
import { CardExperienceOrderListComponent } from './shared/card/card-experience-order-list/card-experience-order-list.component';
import { ListTileComponent } from './shared/list/list-tile/list-tile.component';
import { ListTileIconComponent } from './shared/list/list-tile-icon/list-tile-icon.component';
import { LabelTextLargeComponent } from './shared/label/label-text-large/label-text-large.component';
import { CardExperienceBookingScheduleComponent } from './shared/card/card-experience-booking-schedule/card-experience-booking-schedule.component';
import { CardExperienceBookingContactInfoComponent } from './shared/card/card-experience-booking-contact-info/card-experience-booking-contact-info.component';
import { SidebarExperienceBookingComponent } from './shared/sidebar/sidebar-experience-booking/sidebar-experience-booking.component';
import { LabelTextSmallComponent } from './shared/label/label-text-small/label-text-small.component';
import { CardExperienceBookingSummaryComponent } from './shared/card/card-experience-booking-summary/card-experience-booking-summary.component';
import { CardExperienceBookingPaymentComponent } from './shared/card/card-experience-booking-payment/card-experience-booking-payment.component';
import { HeaderLoginComponent } from './shared/header/login/header-login.component'

import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';

import { SliderProductDetailsComponent } from './shared/slider/slider-product-details/slider-product-details.component';
import { ManageScheduleComponent } from './component/UserModule/manage-experience/product/edit/shared/manage-schedule/manage-schedule.component';
import { CardExperienceCustomerReviewComponent } from './shared/card/card-experience-customer-review/card-experience-customer-review.component';
import { ModalComponent } from './shared/containers/modal/modal.component';
import { AccountComponent } from './component/UserModule/account/account.component';
import { ManageExperienceComponent } from './component/UserModule/manage-experience/manage-experience.component';
import { SearchComponent } from './component/CatalogModule/search/search.component';
import { ListPayoutComponent } from './shared/list/list-payout/list-payout.component';
import { PhotoProfileEditComponent } from './component/UserModule/profile/photo-profile-edit/photo-profile-edit.component';
import { FeaturedPromoBlockComponent } from './shared/block/featured-promo-block/featured-promo-block.component';
import { CardExperiencePromoComponent } from './shared/card/card-experience-promo/card-experience-promo.component';
import { PromoListComponent } from './component/CatalogModule/promo/promo-list/promo-list.component';
import { PromoViewComponent } from './component/CatalogModule/promo/promo-view/promo-view.component';
import { LandingpageComponent } from './component/UserModule/manage-experience/onboarding/landingpage/landingpage.component';
import { OnboardingStep1Component } from './component/UserModule/manage-experience/onboarding/onboarding-step1/onboarding-step1.component';
import { OnboardingStep2Component } from './component/UserModule/manage-experience/onboarding/onboarding-step2/onboarding-step2.component';
import { OnboardingStep3Component } from './component/UserModule/manage-experience/onboarding/onboarding-step3/onboarding-step3.component';

import {MatSnackBarModule} from '@angular/material';



@NgModule({
  declarations: [
    AppComponent,
    PersonalInformationComponent,
    BasicInformationComponent,
    PriceAvailabilityComponent,
    ExperienceDetailsComponent,
    HeaderComponent,
    HomeComponent,
    ProductComponent,
    EditComponent,
    PhotoUploadComponent,
    ListComponent,
    ViewComponent,
    ListExperienceComponent,
    LoginComponent,
    ForgotPasswordComponent,
    RegisterComponent,
    AddComponent,
    BookingDetailsComponent,
    BookingReviewComponent,
    MyOrdersComponent,
    MyOrdersViewComponent,
    MyBookingComponent,
    CalendarrComponent,
    MyProfileComponent,
    HomepageComponent,
    MyExpComponent,
    AnimateComponent,
    CardExperienceProductComponent,
    GlobalHeaderComponent,
    GlobalFooterComponent,
    FeaturedLocationBlockComponent,
    FeaturedProductBlockComponent,
    FeaturedActivityBlockComponent,
    CardExperienceLocationComponent,
    CardExperienceActivityComponent,
    CardExperienceFeaturedProductComponent,
    CardBecomeHostComponent,
    CardExperienceProductDetailsComponent,
    BookingPaymentComponent,
    SidebarAccountComponent,
    SidebarMyExperienceComponent,
    PayoutListComponent,
    PayoutViewComponent,
    BankAccountListComponent,
    BankAccountViewComponent,
    MyProfileEditComponent,
    ChangePasswordComponent,
    MyBookingViewComponent,
    CardExperienceOrderListComponent,
    ListTileComponent,
    ListTileIconComponent,
    LabelTextLargeComponent,
    CardExperienceBookingScheduleComponent,
    CardExperienceBookingContactInfoComponent,
    SidebarExperienceBookingComponent,
    LabelTextSmallComponent,
    CardExperienceBookingSummaryComponent,
    CardExperienceBookingPaymentComponent,
    HeaderLoginComponent,
    SliderProductDetailsComponent,
    HeaderLoginComponent,
    ManageScheduleComponent,
    CardExperienceCustomerReviewComponent,
    ModalComponent,
    AccountComponent,
    ManageExperienceComponent,
    SearchComponent,
    ListPayoutComponent,
    FeaturedPromoBlockComponent,
    CardExperiencePromoComponent,
    PhotoProfileEditComponent,
    PromoListComponent,
    PromoViewComponent,
    LandingpageComponent,
    OnboardingStep1Component,
    OnboardingStep2Component,
    OnboardingStep3Component,
  ],
  imports: [
    BrowserModule,
    FormsModule,
    BrowserAnimationsModule,
    HttpClientModule,
    AppRoutingModule,
    MatCardModule,
    MatRadioModule,
    AngularFontAwesomeModule,
    ReactiveFormsModule,
    MatFormFieldModule,
    MatIconModule,
    MatInputModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MatSelectModule,
    MatDialogModule,
    MatCheckboxModule,
    CalendarModule,
    MatAutocompleteModule,
    MatSnackBarModule,
    FontAwesomeModule,
    BrowserAnimationsModule,
  ],
  entryComponents: [
   
  ],
  providers: [
    FormGuard,
    DatePipe
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
  constructor(){
   
  }
}
